package com.keepcode.keepcodeshop;

import com.keepcode.keepcodeshop.constants.enums.RowStatusCode;
import com.keepcode.keepcodeshop.dto.entities.Category;
import com.keepcode.keepcodeshop.dto.entities.Client;
import com.keepcode.keepcodeshop.dto.entities.Product;
import com.keepcode.keepcodeshop.dto.entities.RowStatus;
import com.keepcode.keepcodeshop.dto.entities.User;
import com.keepcode.keepcodeshop.security.SecurityUser;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.var;
import net.bytebuddy.utility.RandomString;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.token.DefaultToken;
import org.springframework.security.core.token.Token;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static com.keepcode.keepcodeshop.utils.TimeUtils.currentTimestamp;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TestData {
    public static final String USERNAME = "user@test.com";
    public static final String PRODUCT_NAME = "Хлеб";
    public static final String MEASUREMENT_UNITS = "кг.";
    public static final String NAME = "Петров Пётр Петрович";
    public static final String VALID_PASSWORD = "QwErTy12345@";
    public static final String VALID_PHONE = "+79892459289";
    public static final String PASSWORD = "qweryqweryqwerty";
    public static final Long TIME_MILLS = 100000L;
    public static final String ROLE = "ADMIN";
    public static final String CATEGORY_NAME = "Продукты питания";
    public static final Long NUMBER = 1L;

    public static Token generateToken(String username) {
        return generateToken(username, new Date().getTime() + TIME_MILLS);
    }

    public static Token generateToken(String username, Long time) {
        return new DefaultToken(RandomString.make(10), time, username);
    }

    public static UserDetails getUserDetails(String username) {
        return new SecurityUser(username, PASSWORD, getAuthorityList(), true);
    }

    public static User getUser() {
        var user = new User();
        user.setId(NUMBER);
        user.setName(USERNAME);
        user.setPassword(VALID_PASSWORD);
        user.setRole(ROLE);
        user.setAddDate(currentTimestamp());
        user.setLastLoginDate(currentTimestamp());
        return user;
    }

    public static Client getClient() {
        var client = new Client();
        client.setId(NUMBER);
        client.setFullName(NAME);
        client.setContactPhone(VALID_PHONE);
        client.setAddDate(currentTimestamp());
        client.setUserId(NUMBER);
        client.setRowStatus(getRowStatus(RowStatusCode.ACTIVE));
        return client;
    }

    public static Product getProduct() {
        var product = new Product();
        product.setId(NUMBER);
        product.setName(PRODUCT_NAME);
        product.setBalanceUnits(new Random().nextDouble());
        product.setMeasurementUnits(MEASUREMENT_UNITS);
        product.setPrice(new Random().nextDouble());
        product.setRowStatus(getRowStatus(RowStatusCode.ACTIVE));
        product.setOrders(new ArrayList<>());
        product.setCategory(getCategory(NUMBER, CATEGORY_NAME));
        return product;
    }

    public static Category getCategory(Long id, String name) {
        return new Category(id, name);
    }

    public static RowStatus getRowStatus(RowStatusCode rowStatusCode) {
        var rowStatus = new RowStatus(rowStatusCode.name());
        rowStatus.setId(NUMBER);
        return rowStatus;
    }

    private static List<SimpleGrantedAuthority> getAuthorityList() {
        return Collections.singletonList(new SimpleGrantedAuthority(ROLE));
    }
}