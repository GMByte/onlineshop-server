package com.keepcode.keepcodeshop.services.impl;

import com.keepcode.keepcodeshop.constants.enums.RowStatusCode;
import com.keepcode.keepcodeshop.dto.entities.Category;
import com.keepcode.keepcodeshop.dto.entities.Product;
import com.keepcode.keepcodeshop.dto.views.product.ProductCreateView;
import com.keepcode.keepcodeshop.dto.views.product.ProductUpdateOrResponseView;
import com.keepcode.keepcodeshop.exceptions.ApiException;
import com.keepcode.keepcodeshop.exceptions.RequestValidationException;
import com.keepcode.keepcodeshop.repositories.ProductRepository;
import com.keepcode.keepcodeshop.repositories.RowStatusRepository;
import com.keepcode.keepcodeshop.services.CategoryService;
import com.keepcode.keepcodeshop.services.ProductService;
import lombok.SneakyThrows;
import lombok.var;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.keepcode.keepcodeshop.TestData.CATEGORY_NAME;
import static com.keepcode.keepcodeshop.TestData.MEASUREMENT_UNITS;
import static com.keepcode.keepcodeshop.TestData.NUMBER;
import static com.keepcode.keepcodeshop.TestData.PRODUCT_NAME;
import static com.keepcode.keepcodeshop.TestData.getCategory;
import static com.keepcode.keepcodeshop.TestData.getProduct;
import static com.keepcode.keepcodeshop.TestData.getRowStatus;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ProductServiceImplTest {

    private final RowStatusRepository rowStatusRepository;
    private final ProductRepository productRepository;
    private final CategoryService categoryService;
    private final ProductService productService;

    private Product product;

    public ProductServiceImplTest() {
        rowStatusRepository = mock(RowStatusRepository.class);
        productRepository = mock(ProductRepository.class);
        categoryService = mock(CategoryService.class);
        productService = new ProductServiceImpl(rowStatusRepository, productRepository, categoryService);
    }

    @BeforeEach
    private void initData() {
        product = getProduct();
        var category = getCategory(NUMBER, CATEGORY_NAME);

        var statusDeleted = getRowStatus(RowStatusCode.DELETED);
        when(rowStatusRepository.getByCode(any())).thenReturn(of(statusDeleted));
        when(productRepository.findByIdAndAndRowStatusNot(anyLong(), any())).thenReturn(of(product));
        when(categoryService.getNonNullById(anyLong())).thenReturn(category);
    }

    @Test
    @SneakyThrows
    void create() {
        var createView = getProductCreateView();

        when(productRepository.save(any())).thenReturn(product);

        var retrievedProduct = productService.create(createView);

        assertEquals(product, retrievedProduct);
        verify(productRepository, times(1)).save(any());

        createView.setPrice(-1D);

        assertThrows(RequestValidationException.class, () -> productService.create(createView));
    }

    @Test
    @SneakyThrows
    void update() {
        var updateView = getProductUpdateView();

        when(productRepository.save(any())).thenReturn(product);

        var retrievedProduct = productService.update(updateView);

        assertEquals(product, retrievedProduct);
        verify(productRepository, times(1)).save(any());

        when(productRepository.findByIdAndAndRowStatusNot(anyLong(), any())).thenReturn(empty());

        assertThrows(ApiException.class, () -> productService.update(updateView));

        when(productRepository.findByIdAndAndRowStatusNot(anyLong(), any())).thenReturn(of(product));

        var emptyUpdateView = new ProductUpdateOrResponseView();
        emptyUpdateView.setId(NUMBER);

        assertDoesNotThrow(() -> productService.update(emptyUpdateView));
    }

    @Test
    void delete() {
        assertDoesNotThrow(() -> productService.delete(NUMBER));

        when(productRepository.findByIdAndAndRowStatusNot(anyLong(), any())).thenReturn(empty());

        assertThrows(ApiException.class, () -> productService.delete(NUMBER));
    }

    @Test
    void findById() {
        var result = productService.findById(NUMBER);

        assertTrue(productEqualsView(product, result));

        when(productRepository.findByIdAndAndRowStatusNot(anyLong(), any())).thenReturn(empty());

        assertThrows(ApiException.class, () -> productService.findById(NUMBER));
    }

    @Test
    void getPage() {
        var products = getProducts();
        var productPage = new PageImpl<>(products);

        when(productRepository.findProductsByCategoryOptional(anyLong(), any(), any())).thenReturn(productPage);

        var responseList = productService.getPage(NUMBER.intValue(), NUMBER.intValue(), NUMBER);

        assertViewsAndProductsMatch(products, responseList);
    }

    @Test
    void getNonNullById() {
        var resultingProduct = productService.getNonNullById(NUMBER);

        assertEquals(resultingProduct, product);

        when(productRepository.findByIdAndAndRowStatusNot(anyLong(), any())).thenReturn(empty());

        assertThrows(ApiException.class, () -> productService.getNonNullById(NUMBER));
    }

    private List<Product> getProducts() {
        var products = new ArrayList<Product>();
        for (var i = 0; i < 5; i++) {
            var randomProduct = new Product();
            randomProduct.setId(new Random().nextLong());
            randomProduct.setName(RandomString.make(10));
            randomProduct.setMeasurementUnits(RandomString.make(3));
            randomProduct.setBalanceUnits(new Random().nextDouble());
            randomProduct.setCategory(getCategory(new Random().nextLong(), CATEGORY_NAME));
            randomProduct.setRowStatus(getRowStatus(RowStatusCode.ACTIVE));
            randomProduct.setPrice(new Random().nextDouble());
            products.add(randomProduct);
        }
        return products;
    }

    private void assertViewsAndProductsMatch(List<Product> products, List<ProductUpdateOrResponseView> views) {
        assertEquals(products.size(),views.size());
        for (var product : products) {
            var matchingView = views.stream()
                    .filter(view -> productEqualsView(product, view))
                    .findAny();
            assertTrue(matchingView.isPresent());
        }
    }

    private boolean productEqualsView(Product product, ProductUpdateOrResponseView view) {
        return product.getId().equals(view.getId())
                && product.getName().equals(view.getName())
                && product.getPrice().equals(view.getPrice())
                && product.getMeasurementUnits().equals(view.getMeasurementUnits())
                && product.getBalanceUnits().equals(view.getBalance())
                && product.getCategory().getId().equals(view.getCategoryId());
    }

    private ProductCreateView getProductCreateView() {
        return ProductCreateView.builder()
                .name(PRODUCT_NAME)
                .categoryId(NUMBER)
                .price(new Random().nextDouble())
                .measurementUnits(MEASUREMENT_UNITS)
                .balance(new Random().nextDouble())
                .build();
    }

    private ProductUpdateOrResponseView getProductUpdateView() {
        var productUpdateView = new ProductUpdateOrResponseView();
        productUpdateView.setId(NUMBER);
        productUpdateView.setName(PRODUCT_NAME);
        productUpdateView.setCategoryId(NUMBER);
        productUpdateView.setPrice(new Random().nextDouble());
        productUpdateView.setMeasurementUnits(MEASUREMENT_UNITS);
        productUpdateView.setBalance(new Random().nextDouble());
        return productUpdateView;
    }
}