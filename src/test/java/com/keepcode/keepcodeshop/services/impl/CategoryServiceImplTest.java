package com.keepcode.keepcodeshop.services.impl;

import com.keepcode.keepcodeshop.constants.enums.RowStatusCode;
import com.keepcode.keepcodeshop.dto.entities.Category;
import com.keepcode.keepcodeshop.dto.entities.Product;
import com.keepcode.keepcodeshop.dto.entities.RowStatus;
import com.keepcode.keepcodeshop.dto.views.category.CategoryCreateView;
import com.keepcode.keepcodeshop.dto.views.category.CategoryUpdateOrResponseView;
import com.keepcode.keepcodeshop.exceptions.ApiException;
import com.keepcode.keepcodeshop.repositories.CategoryRepository;
import com.keepcode.keepcodeshop.repositories.ProductRepository;
import com.keepcode.keepcodeshop.repositories.RowStatusRepository;
import com.keepcode.keepcodeshop.services.CategoryService;
import lombok.var;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static com.keepcode.keepcodeshop.TestData.CATEGORY_NAME;
import static com.keepcode.keepcodeshop.TestData.NUMBER;
import static com.keepcode.keepcodeshop.TestData.getCategory;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class CategoryServiceImplTest {

    private static final Integer numberOfProducts = 5;

    private final RowStatusRepository rowStatusRepository;
    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;
    private final CategoryService categoryService;

    public CategoryServiceImplTest () {
        rowStatusRepository = mock(RowStatusRepository.class);
        categoryRepository = mock(CategoryRepository.class);
        productRepository = mock(ProductRepository.class);
        categoryService = new CategoryServiceImpl(rowStatusRepository, categoryRepository, productRepository);
    }

    @Test
    void create() {
        var createView = new CategoryCreateView(CATEGORY_NAME);
        var expectedCategory = getCategory(NUMBER, CATEGORY_NAME);

        when(categoryRepository.save(any())).thenReturn(expectedCategory);

        var retrievedCategory = categoryService.create(createView);

        assertEquals(expectedCategory, retrievedCategory);
        verify(categoryRepository, times(1)).save(any());
    }

    @Test
    void update() {
        var categoryOpt = of(getCategory(NUMBER, CATEGORY_NAME));
        var expectedCategory = getCategory(NUMBER, CATEGORY_NAME);

        when(categoryRepository.save(any())).thenReturn(expectedCategory);
        when(categoryRepository.findById(anyLong())).thenReturn(categoryOpt);

        var updateView = new CategoryUpdateOrResponseView(NUMBER, CATEGORY_NAME);
        var retrievedCategory = categoryService.update(updateView);

        assertEquals(expectedCategory, retrievedCategory);
        verify(categoryRepository, times(1)).save(any());
    }

    @Test
    void delete() {
        var category = getCategory(NUMBER, CATEGORY_NAME);

        when(categoryRepository.findById(anyLong())).thenReturn(of(category));
        when(productRepository.findProductsByCategory(any())).thenReturn(getProducts(category, RowStatusCode.ACTIVE));

        assertThrows(ApiException.class, () -> categoryService.delete(NUMBER, null));

        verify(categoryRepository, never()).deleteById(anyLong());

        assertDoesNotThrow(() -> categoryService.delete(NUMBER, NUMBER));

        verify(categoryRepository, times(1)).deleteById(anyLong());

        when(productRepository.findProductsByCategory(any())).thenReturn(getProducts(category, RowStatusCode.DELETED));

        assertDoesNotThrow(() -> categoryService.delete(NUMBER, null));

        verify(categoryRepository, times(2)).deleteById(anyLong());
    }

    @Test
    void findById() {
        var expectedCategory = getCategory(NUMBER, CATEGORY_NAME);

        when(categoryRepository.findById(anyLong())).thenReturn(of(expectedCategory));

        var responseView = categoryService.findById(NUMBER);

        assertEquals(responseView.getId(), expectedCategory.getId());
        assertEquals(responseView.getName(), expectedCategory.getName());
    }

    @Test
    void listCategories() {
        var categories = Arrays.asList(getCategory(NUMBER, CATEGORY_NAME), getCategory(NUMBER + 1, CATEGORY_NAME));

        when(categoryRepository.findAll()).thenReturn(categories);

        var responseList = categoryService.listCategories();

        assertEquals(categories.size(), responseList.size());
        for (var category : categories) {
            var matchingView = responseList.stream()
                    .filter(view -> view.getId().equals(category.getId()) && view.getName().equals(category.getName()))
                    .findAny();
            assertTrue(matchingView.isPresent());
        }
    }

    @Test
    void getNonNullById() {
        var category = getCategory(NUMBER, CATEGORY_NAME);

        when(categoryRepository.findById(anyLong())).thenReturn(of(category));

        var retrievedCategory = categoryService.getNonNullById(NUMBER);

        assertEquals(category, retrievedCategory);
        when(categoryRepository.findById(anyLong())).thenReturn(empty());
        assertThrows(ApiException.class, () -> categoryService.getNonNullById(NUMBER));
    }

    private List<Product> getProducts(Category category, RowStatusCode rowStatusCode) {
        var products = new ArrayList<Product>();
        for (var i = 0; i < numberOfProducts; i++) {
            var product = new Product();
            product.setId(new Random().nextLong());
            product.setCategory(category);
            product.setRowStatus(new RowStatus(rowStatusCode.name()));
            products.add(product);
        }
        return products;
    }
}