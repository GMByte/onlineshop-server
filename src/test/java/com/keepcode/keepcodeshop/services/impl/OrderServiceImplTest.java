package com.keepcode.keepcodeshop.services.impl;

import com.keepcode.keepcodeshop.constants.enums.RowStatusCode;
import com.keepcode.keepcodeshop.dto.entities.Client;
import com.keepcode.keepcodeshop.dto.entities.Order;
import com.keepcode.keepcodeshop.dto.entities.ProductsInOrders;
import com.keepcode.keepcodeshop.dto.entities.User;
import com.keepcode.keepcodeshop.dto.views.order.OrderCreateView;
import com.keepcode.keepcodeshop.dto.views.order.OrderResponseView;
import com.keepcode.keepcodeshop.dto.views.order.OrderUpdateView;
import com.keepcode.keepcodeshop.dto.views.order.ProductToOrderView;
import com.keepcode.keepcodeshop.exceptions.ApiException;
import com.keepcode.keepcodeshop.exceptions.RequestValidationException;
import com.keepcode.keepcodeshop.repositories.OrderRepository;
import com.keepcode.keepcodeshop.repositories.ProductOrderRepository;
import com.keepcode.keepcodeshop.repositories.RowStatusRepository;
import com.keepcode.keepcodeshop.services.ClientService;
import com.keepcode.keepcodeshop.services.OrderService;
import com.keepcode.keepcodeshop.services.ProductService;
import lombok.SneakyThrows;
import lombok.var;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static com.keepcode.keepcodeshop.TestData.NUMBER;
import static com.keepcode.keepcodeshop.TestData.getClient;
import static com.keepcode.keepcodeshop.TestData.getProduct;
import static com.keepcode.keepcodeshop.TestData.getRowStatus;
import static com.keepcode.keepcodeshop.TestData.getUser;
import static com.keepcode.keepcodeshop.utils.TimeUtils.currentTimestamp;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class OrderServiceImplTest {

    private final RowStatusRepository rowStatusRepository;
    private final OrderRepository orderRepository;
    private final ProductService productService;
    private final ProductOrderRepository productOrderRepository;
    private final ClientService clientService;
    private final OrderService orderService;

    private Client client;
    private User user;
    private Order order;

    public OrderServiceImplTest() {
        rowStatusRepository = mock(RowStatusRepository.class);
        orderRepository = mock(OrderRepository.class);
        productService = mock(ProductService.class);
        productOrderRepository = mock(ProductOrderRepository.class);
        clientService = mock(ClientService.class);
        orderService = new OrderServiceImpl(rowStatusRepository, orderRepository, productService,
                productOrderRepository, clientService);
    }

    @BeforeEach
    private void initData() {
        client = getClient();
        user = getUser();
        order = getRandomOrder();

        client.setUserId(user.getId());

        var rowStatusDeleted = getRowStatus(RowStatusCode.DELETED);
        when(rowStatusRepository.getByCode(any())).thenReturn(of(rowStatusDeleted));
        when(orderRepository.getOrderByIdAndRowStatusNot(anyLong(), any())).thenReturn(of(order));
    }

    @Test
    @SneakyThrows
    void create() {
        when(clientService.getNonNullById(anyLong())).thenReturn(client);
        when(orderRepository.save(any())).thenReturn(order);
        when(rowStatusRepository.getByCode(any())).thenReturn(of(getRowStatus(RowStatusCode.ACTIVE)));
        when(productService.getNonNullById(anyLong())).thenReturn(getProduct());

        var orderCreateView = getOrderCreateView(client.getId());
        orderService.create(orderCreateView, user);

        verify(orderRepository, times(2)).save(any());
        verify(productOrderRepository, times(orderCreateView.getItems().size())).save(any());

        client.setUserId(user.getId() + 1);

        assertThrows(ApiException.class, () -> orderService.create(orderCreateView, user));
    }

    @Test
    void update() {
        var updateView = getOrderUpdateView();

        orderService.update(updateView);

        verify(orderRepository, times(1)).save(any());

        var emptyUpdateView = OrderUpdateView.builder().build();

        when(orderRepository.getOrderByIdAndRowStatusNot(anyLong(), any())).thenReturn(empty());

        assertThrows(ApiException.class, () -> orderService.update(updateView));

        when(orderRepository.getOrderByIdAndRowStatusNot(anyLong(), any())).thenReturn(of(order));
        emptyUpdateView.setId(NUMBER);

        assertDoesNotThrow(() -> orderService.update(emptyUpdateView));
    }

    @Test
    void addProductToOrder() {
        order.setRowStatus(getRowStatus(RowStatusCode.CREATED));
        var productToOrder = getProductToOrderView();

        when(productService.getNonNullById(anyLong())).thenReturn(getProduct());

        assertDoesNotThrow(() -> orderService.addProductToOrder(productToOrder, user));

        verify(productOrderRepository, times(1)).save(any());
        verify(orderRepository, times(1)).save(any());

        order.getClient().setUserId(user.getId() + 1);

        assertThrows(ApiException.class, () -> orderService.addProductToOrder(productToOrder, user));

        order.getClient().setUserId(user.getId());
        order.setRowStatus(getRowStatus(RowStatusCode.MANUAL));

        assertThrows(RequestValidationException.class, () -> orderService.addProductToOrder(productToOrder, user));
    }

    @Test
    void listOrders() {
        var orders = getRandomOrderList();

        when(orderRepository.findOrdersByUserIdAndNotInStatus(anyLong(), anyLong())).thenReturn(orders);
        when(productOrderRepository.getProductsByOrderId(anyLong())).thenReturn(getProductsInOrder());

        var responseList = orderService.listOrders(user);

        assertViewsAndClientsMatch(orders, responseList);
    }

    @Test
    void deleteProductFromOrder() {
        when(productService.getNonNullById(anyLong())).thenReturn(getProduct());

        assertDoesNotThrow(() -> orderService.deleteProductFromOrder(NUMBER, NUMBER, user));

        client.setUserId(user.getId() + 1);
        order.setClient(client);

        assertThrows(ApiException.class, () -> orderService.deleteProductFromOrder(NUMBER, NUMBER, user));

        client.setUserId(user.getId());
        order.setRowStatus(getRowStatus(RowStatusCode.MANUAL));

        assertThrows(RequestValidationException.class, () -> orderService.deleteProductFromOrder(NUMBER, NUMBER, user));
    }

    @Test
    void cancel() {
        assertDoesNotThrow(() -> orderService.cancel(NUMBER, user));

        client.setUserId(user.getId() + 1);
        order.setClient(client);

        assertThrows(ApiException.class, () -> orderService.cancel(NUMBER, user));

        client.setUserId(user.getId());
        order.setRowStatus(getRowStatus(RowStatusCode.MANUAL));

        assertThrows(RequestValidationException.class, () -> orderService.cancel(NUMBER, user));

        when(orderRepository.getOrderByIdAndRowStatusNot(anyLong(), any())).thenReturn(empty());

        assertThrows(ApiException.class, () -> orderService.delete(NUMBER));
    }

    @Test
    void delete() {
        assertDoesNotThrow(() -> orderService.delete(NUMBER));

        when(orderRepository.getOrderByIdAndRowStatusNot(anyLong(), any())).thenReturn(empty());

        assertThrows(ApiException.class, () -> orderService.delete(NUMBER));
    }

    @Test
    void findOrdersByUser() {
        var orders = getRandomOrderList();

        when(orderRepository.findOrdersByUserIdAndNotInStatus(anyLong(), anyLong())).thenReturn(orders);
        when(productOrderRepository.getProductsByOrderId(anyLong())).thenReturn(getProductsInOrder());

        var responseList = orderService.findOrdersByUser(NUMBER);

        assertViewsAndClientsMatch(orders, responseList);
    }

    @Test
    void findOrdersByClient() {
        var orders = getRandomOrderList();

        when(orderRepository.findOrdersByClientAndNotInStatus(anyLong(), anyLong())).thenReturn(orders);
        when(productOrderRepository.getProductsByOrderId(anyLong())).thenReturn(getProductsInOrder());

        var responseList = orderService.findOrdersByClient(NUMBER);

        assertViewsAndClientsMatch(orders, responseList);
    }

    @Test
    void getNonNullById() {
        var retrievedOrder = orderService.getNonNullById(NUMBER);

        assertEquals(order, retrievedOrder);

        when(orderRepository.getOrderByIdAndRowStatusNot(anyLong(), any())).thenReturn(empty());

        assertThrows(ApiException.class, () -> orderService.getNonNullById(NUMBER));
    }

    private List<Order> getRandomOrderList() {
        var orders = new ArrayList<Order>();
        for (var i = 0; i < 5; i++) {
            orders.add(getRandomOrder());
        }
        return orders;
    }

    private void assertViewsAndClientsMatch(List<Order> orders, List<OrderResponseView> views) {
        assertEquals(orders.size(),views.size());
        for (var order : orders) {
            var matchingView = views.stream()
                    .filter(view -> orderEqualsView(order, view))
                    .findAny();
            assertTrue(matchingView.isPresent());
        }
    }

    private boolean orderEqualsView(Order order, OrderResponseView view) {
        return order.getId().equals(view.getId()) && order.getPriceTotal().equals(view.getPriceTotal())
                && order.getNumber().equals(view.getNumber()) && order.getPostNumber().equals(view.getPostNumber())
                && order.getRowStatus().getCode().equals(view.getRowStatusCode()) && order.getClient().getId().equals(view.getClientId())
                && order.getProducts().size() == view.getProductsOnOrder().size();
    }

    private List<ProductsInOrders> getProductsInOrder() {
       var productsInOrder = new ProductsInOrders();
       var orderProductKey = new ProductsInOrders.OrderProductKey();
       orderProductKey.setOrder(getRandomOrder());
       orderProductKey.setProduct(getProduct());
       productsInOrder.setOrderProductKey(orderProductKey);
       productsInOrder.setProductAmount(new Random().nextDouble());
       return Collections.singletonList(productsInOrder);
    }

    private ProductToOrderView getProductToOrderView() {
        return ProductToOrderView.builder()
                .orderId(NUMBER)
                .productId(NUMBER)
                .amount(new Random().nextDouble())
                .build();
    }

    private OrderUpdateView getOrderUpdateView() {
        return OrderUpdateView.builder()
                .id(NUMBER)
                .clientId(NUMBER)
                .postNumber(RandomString.make(10))
                .rowStatusCode(getRowStatus(RowStatusCode.ACTIVE).getCode())
                .build();
    }

    private Order getRandomOrder() {
        var order = new Order();
        order.setId(new Random().nextLong());
        order.setNumber(RandomString.make(10));
        order.setPostNumber(RandomString.make(10));
        order.setProducts(Collections.singletonList(getProduct()));
        order.setRowStatus(getRowStatus(RowStatusCode.CREATED));
        order.setPriceTotal(new Random().nextDouble());
        order.setClient(getClient());
        order.setAddDate(currentTimestamp());
        return order;
    }

    private OrderCreateView getOrderCreateView(Long clientId) {
        var orderCreateView = OrderCreateView.builder()
                .clientId(clientId)
                .build();
        var items = new ArrayList<OrderCreateView.Item>();
        for (var i = 0; i < 5; i++) {
            var item = OrderCreateView.Item.builder()
                    .productId(new Random().nextLong())
                    .amount(new Random().nextDouble())
                    .build();
            items.add(item);
        }
        orderCreateView.setItems(items);
        return orderCreateView;
    }
}