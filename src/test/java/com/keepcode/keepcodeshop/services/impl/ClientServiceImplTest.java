package com.keepcode.keepcodeshop.services.impl;

import com.keepcode.keepcodeshop.constants.enums.RowStatusCode;
import com.keepcode.keepcodeshop.dto.entities.Client;
import com.keepcode.keepcodeshop.dto.entities.User;
import com.keepcode.keepcodeshop.dto.views.client.ClientCreateView;
import com.keepcode.keepcodeshop.dto.views.client.ClientUpdateOrResponseView;
import com.keepcode.keepcodeshop.exceptions.ApiException;
import com.keepcode.keepcodeshop.exceptions.RequestValidationException;
import com.keepcode.keepcodeshop.repositories.ClientRepository;
import com.keepcode.keepcodeshop.repositories.RowStatusRepository;
import com.keepcode.keepcodeshop.services.ClientService;
import lombok.SneakyThrows;
import lombok.var;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.keepcode.keepcodeshop.TestData.NAME;
import static com.keepcode.keepcodeshop.TestData.NUMBER;
import static com.keepcode.keepcodeshop.TestData.TIME_MILLS;
import static com.keepcode.keepcodeshop.TestData.VALID_PHONE;
import static com.keepcode.keepcodeshop.TestData.getClient;
import static com.keepcode.keepcodeshop.TestData.getRowStatus;
import static com.keepcode.keepcodeshop.TestData.getUser;
import static com.keepcode.keepcodeshop.utils.TimeUtils.currentTimestamp;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ClientServiceImplTest {

    private final String INVALID_PHONE = "+721wq293021111";

    private final RowStatusRepository rowStatusRepository;
    private final ClientRepository clientRepository;
    private final ClientService clientService;

    private User user;
    private Client client;

    public ClientServiceImplTest() {
        rowStatusRepository = mock(RowStatusRepository.class);
        clientRepository = mock(ClientRepository.class);
        clientService = new ClientServiceImpl(rowStatusRepository, clientRepository);
    }

    @BeforeEach
    private void initData() {
        user = getUser();
        client = getClient();

        var rowStatusDeleted = getRowStatus(RowStatusCode.DELETED);
        when(rowStatusRepository.getByCode(RowStatusCode.DELETED.name())).thenReturn(of(rowStatusDeleted));
        when(clientRepository.getClientByIdAndRowStatusNot(anyLong(), any())).thenReturn(of(client));
    }

    @Test
    @SneakyThrows
    void create() {
        var clientCreateView = new ClientCreateView(NAME, VALID_PHONE);

        when(rowStatusRepository.getByCode(any())).thenReturn(of(getRowStatus(RowStatusCode.ACTIVE)));
        when(clientRepository.save(any())).thenReturn(buildClient(clientCreateView, user));

        assertDoesNotThrow(() -> clientService.create(clientCreateView, user));

        verify(clientRepository, times(1)).save(any());

        clientCreateView.setContactPhone(INVALID_PHONE);

        assertThrows(RequestValidationException.class, () -> clientService.create(clientCreateView, user));
    }

    @Test
    void update() {
        var clientUpdateView = getClientUpdateView();

        when(clientRepository.save(any())).thenReturn(client);

        assertDoesNotThrow(() -> clientService.update(clientUpdateView, user));

        verify(clientRepository, times(1)).save(any());

        clientUpdateView.setFullName(null);
        clientUpdateView.setContactPhone(null);

        assertDoesNotThrow(() -> clientService.update(clientUpdateView, user));

        clientUpdateView.setContactPhone(INVALID_PHONE);

        assertThrows(RequestValidationException.class, () -> clientService.update(clientUpdateView, user));

        user.setId(1L);
        client.setUserId(2L);

        assertThrows(ApiException.class, () -> clientService.update(clientUpdateView, user));
    }

    @Test
    void delete() {
        assertDoesNotThrow(() -> clientService.delete(NUMBER, user));
    }

    @Test
    void listUserClients() {
        var clients = getRandomClientList();

        when(clientRepository.getClientsByUserIdAndRowStatusNot(anyLong(), any())).thenReturn(clients);

        var responseList = clientService.listUserClients(user);

        assertViewsAndClientsMatch(clients, responseList);
    }

    @Test
    void listClients() {
        var clients = getRandomClientList();

        when(clientRepository.getClientsByRowStatusNot(any())).thenReturn(clients);

        var responseList = clientService.listClients();

        assertViewsAndClientsMatch(clients, responseList);
    }

    @Test
    void getNonNullById() {
        var retrievedClient = clientService.getNonNullById(NUMBER);

        assertEquals(client, retrievedClient);

        when(clientRepository.getClientByIdAndRowStatusNot(anyLong(), any())).thenReturn(empty());

        assertThrows(ApiException.class, () -> clientService.getNonNullById(NUMBER));
    }

    private void assertViewsAndClientsMatch(List<Client> clients, List<ClientUpdateOrResponseView> views) {
        assertEquals(clients.size(),views.size());
        for (var client : clients) {
            var matchingView = views.stream()
                    .filter(view -> clientEqualsView(client, view))
                    .findAny();
            assertTrue(matchingView.isPresent());
        }
    }

    private boolean clientEqualsView(Client client, ClientUpdateOrResponseView view) {
        return client.getId().equals(view.getId())
                && client.getFullName().equals(view.getFullName())
                && client.getContactPhone().equals(view.getContactPhone())
                && client.getAddDate().equals(view.getAddDate())
                && client.getUserId().equals(view.getUserId());
    }

    private Client buildClient(ClientCreateView view, User user) {
        var client = new Client();
        client.setUserId(user.getId());
        client.setFullName(view.getFullName());
        client.setContactPhone(view.getContactPhone());
        client.setAddDate(currentTimestamp());
        return client;
    }

    private List<Client> getRandomClientList() {
        var clients = new ArrayList<Client>();
        for (var i = 0; i < 5; i++) {
            var randomClient = new Client();
            randomClient.setId(new Random().nextLong());
            randomClient.setUserId(new Random().nextLong());
            randomClient.setFullName(RandomString.make(10));
            randomClient.setContactPhone(RandomString.make(10));
            randomClient.setRowStatus(getRowStatus(RowStatusCode.ACTIVE));
            randomClient.setAddDate(currentTimestamp());
            clients.add(randomClient);
        }
        return clients;
    }

    private ClientUpdateOrResponseView getClientUpdateView() {
        return new ClientUpdateOrResponseView(NUMBER, NAME,
                VALID_PHONE, NUMBER, null);
    }
}