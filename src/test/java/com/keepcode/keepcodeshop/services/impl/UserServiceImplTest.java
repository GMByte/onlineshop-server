package com.keepcode.keepcodeshop.services.impl;

import com.keepcode.keepcodeshop.constants.enums.Role;
import com.keepcode.keepcodeshop.dto.entities.User;
import com.keepcode.keepcodeshop.dto.views.auth.UserCreateView;
import com.keepcode.keepcodeshop.dto.views.user.UserPasswordChangeView;
import com.keepcode.keepcodeshop.dto.views.user.UserResponseView;
import com.keepcode.keepcodeshop.dto.views.user.UserRoleAssignView;
import com.keepcode.keepcodeshop.exceptions.ApiException;
import com.keepcode.keepcodeshop.exceptions.RequestValidationException;
import com.keepcode.keepcodeshop.repositories.RowStatusRepository;
import com.keepcode.keepcodeshop.repositories.UserRepository;
import com.keepcode.keepcodeshop.services.UserService;
import lombok.SneakyThrows;
import lombok.var;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.keepcode.keepcodeshop.TestData.NUMBER;
import static com.keepcode.keepcodeshop.TestData.ROLE;
import static com.keepcode.keepcodeshop.TestData.USERNAME;
import static com.keepcode.keepcodeshop.TestData.VALID_PASSWORD;
import static com.keepcode.keepcodeshop.TestData.getUser;
import static com.keepcode.keepcodeshop.utils.TimeUtils.currentTimestamp;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class UserServiceImplTest {

    private static final String INVALID_PASSWORD = "INVALID";
    private static final String INVALID_ROLE = "INVALID";

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final UserService userService;

    private User user;

    public UserServiceImplTest() {
        var rowStatusRepository = mock(RowStatusRepository.class);
        userRepository = mock(UserRepository.class);
        bCryptPasswordEncoder = mock(BCryptPasswordEncoder.class);
        userService = new UserServiceImpl(rowStatusRepository, userRepository, bCryptPasswordEncoder);
    }

    @BeforeEach
    private void initData() {
        user = getUser();

        when(bCryptPasswordEncoder.encode(anyString())).thenReturn(RandomString.make(10));
    }

    @Test
    void list() {
        var users = getUsers();

        when(userRepository.findAll()).thenReturn(users);

        var resultList = userService.list();

        assertViewsAndUsersMatch(users, resultList);
    }

    @Test
    @SneakyThrows
    void create() {
        when(userRepository.getUserByName(anyString())).thenReturn(null);

        var createView = new UserCreateView(USERNAME, VALID_PASSWORD);

        assertDoesNotThrow(() -> userService.create(createView));
        verify(userRepository, times(1)).save(any());

        when(userRepository.getUserByName(anyString())).thenReturn(user);

        assertThrows(RequestValidationException.class, () -> userService.create(createView));

        when(userRepository.getUserByName(anyString())).thenReturn(null);
        createView.setEmail("INVALID");

        assertThrows(RequestValidationException.class, () -> userService.create(createView));

        createView.setEmail(USERNAME);
        createView.setPassword(INVALID_PASSWORD);

        assertThrows(RequestValidationException.class, () -> userService.create(createView));
    }

    @Test
    @SneakyThrows
    void changePassword() {
        when(bCryptPasswordEncoder.matches(anyString(), anyString())).thenReturn(true);

        var newPassword = VALID_PASSWORD + "1";
        var changePassView = new UserPasswordChangeView(VALID_PASSWORD, newPassword, newPassword);

        assertDoesNotThrow(() -> userService.changePassword(changePassView, user));

        verify(userRepository, times(1)).save(any());

        when(bCryptPasswordEncoder.matches(anyString(), anyString())).thenReturn(false);

        assertThrows(RequestValidationException.class, () -> userService.changePassword(changePassView, user));

        when(bCryptPasswordEncoder.matches(anyString(), anyString())).thenReturn(true);
        changePassView.setConfirmNewPassword(VALID_PASSWORD + "2");

        assertThrows(RequestValidationException.class, () -> userService.changePassword(changePassView, user));

        changePassView.setNewPassword(INVALID_PASSWORD);
        changePassView.setConfirmNewPassword(INVALID_PASSWORD);

        assertThrows(RequestValidationException.class, () -> userService.changePassword(changePassView, user));

        changePassView.setNewPassword(VALID_PASSWORD);
        assertThrows(RequestValidationException.class, () -> userService.changePassword(changePassView, user));
    }

    @Test
    @SneakyThrows
    void assignUserRole() {
        when(userRepository.findById(anyLong())).thenReturn(of(user));

        var userRoleAssignView = new UserRoleAssignView(NUMBER, ROLE);

        assertDoesNotThrow(() -> userService.assignUserRole(userRoleAssignView));

        when(userRepository.findById(anyLong())).thenReturn(empty());

        assertThrows(ApiException.class, () -> userService.assignUserRole(userRoleAssignView));

        when(userRepository.findById(anyLong())).thenReturn(of(user));
        userRoleAssignView.setRole(INVALID_ROLE);

        assertThrows(RequestValidationException.class, () -> userService.assignUserRole(userRoleAssignView));
    }

    @Test
    void getUserByUsername() {
        when(userRepository.getUserByName(anyString())).thenReturn(user);

        var result = userService.getUserByUsername(anyString());

        assertNotNull(result);
        assertEquals(result, user);
    }

    @Test
    void getNonNullById() {
        when(userRepository.findById(anyLong())).thenReturn(of(user));

        var resultingUser = userService.getNonNullById(NUMBER);

        assertEquals(resultingUser, user);

        when(userRepository.findById(anyLong())).thenReturn(empty());

        assertThrows(ApiException.class, () -> userService.getNonNullById(NUMBER));
    }

    private List<User> getUsers() {
        var users = new ArrayList<User>();
        for (var i = 0; i < 5; i++) {
            var user = new User();
            user.setId(new Random().nextLong());
            user.setName(RandomString.make(10));
            user.setRole(Role.USER.name());
            user.setLastLoginDate(currentTimestamp());
            user.setAddDate(currentTimestamp());
            users.add(user);
        }
        return users;
    }

    private void assertViewsAndUsersMatch(List<User> users, List<UserResponseView> views) {
        assertEquals(users.size(),views.size());
        for (var user : users) {
            var matchingView = views.stream()
                    .filter(view -> userEqualsView(user, view))
                    .findAny();
            assertTrue(matchingView.isPresent());
        }
    }

    private boolean userEqualsView(User user, UserResponseView view) {
        return user.getId().equals(view.getId())
                && user.getName().equals(view.getUsername())
                && user.getRole().name().equals(view.getRole())
                && user.getLastLoginDate().equals(view.getLastLoginDate())
                && user.getAddDate().equals(view.getAddDate());
    }
}