package com.keepcode.keepcodeshop.security.managers.impl;

import com.keepcode.keepcodeshop.exceptions.TokenException;
import lombok.Getter;
import lombok.var;
import org.springframework.security.core.token.Token;
import org.springframework.security.core.token.TokenService;

import java.util.Date;

import static com.keepcode.keepcodeshop.TestData.TIME_MILLS;
import static com.keepcode.keepcodeshop.TestData.USERNAME;
import static com.keepcode.keepcodeshop.TestData.generateToken;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

abstract class AbstractMapStoredUserTokenManagerTest extends MapStoredUserTokenManager {

    @Getter
    protected final TokenService tokenService;

    public AbstractMapStoredUserTokenManagerTest(TokenService tokenService) {
        super(tokenService);
        this.tokenService = tokenService;
    }

    void generateAndStoreToken() {
       var username = USERNAME;
       var expectedToken = generateToken(username);

       when(tokenService.allocateToken(anyString())).thenReturn(expectedToken);

       var token = generateAndStoreToken(username);

       assertEquals(expectedToken, token);
       assertTrue(getTokenMap().containsValue(expectedToken));
       assertNotNull(getTokenMap().get(expectedToken.getKey()));
    }

    void removeToken() {
       var token = putTokenForUser(USERNAME);
       removeToken(token.getKey());
       assertTokenNotStored(token);
    }

    void removeTokenByUsername() {
        var username = USERNAME;
        var token = putTokenForUser(username);
        removeTokenByUsername(username);
        assertTokenNotStored(token);
    }

    void validateAndGetUsername() {
        var username = USERNAME;
        var token = putTokenForUser(username);

        when(tokenService.verifyToken(anyString())).thenReturn(token);

        var tokenKey = token.getKey();
        assertDoesNotThrow(() -> validateAndGetUsername(tokenKey));
        var retrievedUsername = validateAndGetUsername(tokenKey);
        assertEquals(username, retrievedUsername);

        getTokenMap().clear();

        var expiredToken = generateToken(username, new Date().getTime() - TIME_MILLS * 2);
        getTokenMap().put(expiredToken.getKey(), expiredToken);

        when(tokenService.verifyToken(anyString())).thenReturn(expiredToken);

        var expiredTokenKey = expiredToken.getKey();

        assertThrows(TokenException.class, () -> validateAndGetUsername(expiredTokenKey));
    }

    protected Token putTokenForUser(String username) {
        var token = generateToken(username);
        getTokenMap().put(token.getKey(), token);
        return token;
    }

    protected void assertTokenNotStored(Token token) {
        assertFalse(getTokenMap().containsKey(token.getKey()));
        assertFalse(getTokenMap().containsValue(token));
    }
}