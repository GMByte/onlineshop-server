package com.keepcode.keepcodeshop.security.managers.impl;

import com.keepcode.keepcodeshop.configuration.SecurityTokenConfig;
import com.keepcode.keepcodeshop.exceptions.TokenException;
import lombok.var;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.token.Token;
import org.springframework.security.core.token.TokenService;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.keepcode.keepcodeshop.TestData.TIME_MILLS;
import static com.keepcode.keepcodeshop.TestData.USERNAME;
import static com.keepcode.keepcodeshop.TestData.generateToken;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class RefreshTokenManagerImplTest extends AbstractMapStoredUserTokenManagerTest {

    private final RefreshTokenManagerImpl refreshTokenManager;
    private final SecurityTokenConfig securityTokenConfig;
    private final Map<String, Token> tokenMap = new ConcurrentHashMap<>();

    public RefreshTokenManagerImplTest() {
        super(mock(TokenService.class));
        securityTokenConfig = mock(SecurityTokenConfig.class);
        refreshTokenManager = new RefreshTokenManagerImpl(getTokenService(), securityTokenConfig);
    }

    @AfterEach
    void clearMap() {
        getTokenMap().clear();
    }

    @Test
    void generateAndStoreToken() {
        super.generateAndStoreToken();
    }

    @Test
    void removeToken() {
        super.removeToken();
    }

    @Test
    void removeTokenByUsername() {
        super.removeTokenByUsername();
    }

    @Test
    void validateAndGetUsername() {
        super.validateAndGetUsername();
    }

    @Test
    void updateAndGetNewIfValid() {
        var firstToken = generateToken(USERNAME);

        when(securityTokenConfig.getDesiredRefreshExpTimeInMinutes()).thenReturn(TIME_MILLS);
        when(tokenService.verifyToken(anyString())).thenReturn(firstToken);
        when(tokenService.allocateToken(anyString())).thenReturn(firstToken);

        var token = refreshTokenManager.generateAndStoreToken(USERNAME);

        var expectedNewToken = generateToken(USERNAME);

        when(tokenService.allocateToken(anyString())).thenReturn(expectedNewToken);

        var newToken = refreshTokenManager.updateAndGetNewIfValid(token.getKey());
        assertEquals(expectedNewToken, newToken);
        var oldTokenKey = token.getKey();

        when(tokenService.verifyToken(anyString())).thenReturn(expectedNewToken);

        assertThrows(TokenException.class, () -> refreshTokenManager.updateAndGetNewIfValid(oldTokenKey));
    }

    @Override
    protected Map<String, Token> getTokenMap() {
        return tokenMap;
    }

    @Override
    protected Long getExpTimeMills() {
        return TIME_MILLS;
    }
}