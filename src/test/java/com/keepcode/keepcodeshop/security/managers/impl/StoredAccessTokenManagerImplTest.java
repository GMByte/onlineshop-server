package com.keepcode.keepcodeshop.security.managers.impl;

import com.keepcode.keepcodeshop.configuration.SecurityTokenConfig;
import com.keepcode.keepcodeshop.exceptions.TokenException;
import lombok.var;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.token.Token;
import org.springframework.security.core.token.TokenService;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.keepcode.keepcodeshop.TestData.TIME_MILLS;
import static com.keepcode.keepcodeshop.TestData.USERNAME;
import static com.keepcode.keepcodeshop.TestData.generateToken;
import static com.keepcode.keepcodeshop.TestData.getUserDetails;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class StoredAccessTokenManagerImplTest extends AbstractMapStoredUserTokenManagerTest {

    private final StoredAccessTokenManagerImpl storedAccessTokenManager;
    private final UserDetailsService userDetailsService;
    private final SecurityTokenConfig securityTokenConfig;
    private final Map<String, Token> tokenMap = new ConcurrentHashMap<>();

    public StoredAccessTokenManagerImplTest() {
        super(mock(TokenService.class));
        userDetailsService = mock(UserDetailsService.class);
        securityTokenConfig = mock(SecurityTokenConfig.class);
        storedAccessTokenManager = new StoredAccessTokenManagerImpl(getTokenService(), userDetailsService, securityTokenConfig);
    }

    @AfterEach
    void clearMap() {
        getTokenMap().clear();
    }

    @Test
    @Override
    void generateAndStoreToken() {
        super.generateAndStoreToken();
    }

    @Test
    @Override
    void removeToken() {
        super.removeToken();
    }

    @Test
    @Override
    void removeTokenByUsername() {
        super.removeTokenByUsername();
    }

    @Test
    @Override
    void validateAndGetUsername() {
        super.validateAndGetUsername();
    }

    @Test
    void validateAndGetAuthentication() {
        var username = USERNAME;
        var token = generateToken(username);
        var tokenKey = token.getKey();
        var userDetails = getUserDetails(username);

        when(securityTokenConfig.getDesiredBearerExpTimeInMinutes()).thenReturn(TIME_MILLS);
        when(tokenService.allocateToken(anyString())).thenReturn(token);
        when(tokenService.verifyToken(anyString())).thenReturn(token);
        when(userDetailsService.loadUserByUsername(anyString())).thenReturn(userDetails);

        storedAccessTokenManager.generateAndStoreToken(username);

        assertDoesNotThrow(() -> storedAccessTokenManager.validateAndGetAuthentication(tokenKey));

        var authentication = storedAccessTokenManager.validateAndGetAuthentication(tokenKey);

        assertNotNull(authentication);
        assertEquals(authentication.getName(), username);

        storedAccessTokenManager.removeToken(tokenKey);

        assertThrows(TokenException.class, () -> storedAccessTokenManager.validateAndGetAuthentication(tokenKey));
    }

    @Override
    protected Map<String, Token> getTokenMap() {
        return tokenMap;
    }

    @Override
    protected Long getExpTimeMills() {
        return TIME_MILLS;
    }
}