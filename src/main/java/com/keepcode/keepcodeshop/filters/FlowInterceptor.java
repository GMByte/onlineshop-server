package com.keepcode.keepcodeshop.filters;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

import static com.keepcode.keepcodeshop.tracing.TraceIdContextHolder.getTraceId;
import static com.keepcode.keepcodeshop.tracing.TraceIdContextHolder.setTraceId;

@Component
@RequiredArgsConstructor
@Slf4j
public class FlowInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response,
                             @NonNull Object handler) {
        setTraceId(UUID.randomUUID().toString());
        MDC.put("reqTraceId", getTraceId());
        return true;
    }
}