package com.keepcode.keepcodeshop.exceptions;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class RequestValidationException extends Exception implements BaseException {

    private static final Integer REQUEST_VALIDATION_EXCEPTION_CODE = -2;

    private final String errorMessage;

    public RequestValidationException(ErrorMessage messageEnum) {
        this.errorMessage = messageEnum.getMessage();
    }

    @Override
    public Integer getCode() {
        return REQUEST_VALIDATION_EXCEPTION_CODE;
    }

    @Getter
    @RequiredArgsConstructor
    public enum ErrorMessage {
        INVALID_PHONE_FORMAT( "Указан невалидный номер телефона"),
        INVALID_EMAIL_FORMAT("Указан невалидный адрес электронной почты"),
        INVALID_PASSWORD_FORMAT("Пароль должен быть минимум 8 символов, содержать хотя бы одну цифру, а также маленькую и заглавную букву, и специальный символ, и не должен содержать пробелов"),
        PASSWORDS_CONFIRMATION_FAILED("Пароли не совпадают"),
        OLD_PASSWORD_WRONG("Старый пароль указан неверно"),
        NEW_PASSWORD_MATCHES_OLD("Новый пароль должен отличаться от старого"),
        EMAIL_ALREADY_REGISTERED("Данный email уже зарегистрирован"),
        NEGATIVE_PRICE_VALUE("Для цены недопустимы значения меньше нуля"),
        INVALID_USERNAME_OR_PASSWORD("Неправильно введён логин или пароль"),
        ORDER_IS_BEING_PROCESSED("Заказ уже взят в обработку"),
        WRONG_ROLE_SPECIFIED("Указана неверная роль");

        private final String message;
    }
}
