package com.keepcode.keepcodeshop.exceptions;

public interface BaseException {
    Integer getCode();
    String getErrorMessage();
}
