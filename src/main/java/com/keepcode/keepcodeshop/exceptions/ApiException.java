package com.keepcode.keepcodeshop.exceptions;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
public class ApiException extends RuntimeException implements BaseException {

    private static final Integer API_EXCEPTION_CODE = -1;
    private final String errorMessage;

    public ApiException(ErrorMessage messageEnum) {
        this.errorMessage = messageEnum.getMessage();
    }

    public ApiException(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public Integer getCode() {
        return API_EXCEPTION_CODE;
    }

    @Getter
    @RequiredArgsConstructor
    public enum ErrorMessage {
        INVALID_LOGIN("Ошибка авторизации"),
        UNKNOWN_ERROR("Неизвестная ошибка"),
        MOVE_TO_ID_NOT_PROVIDED("В категории находятся %s товар(ов). Укажите параметр move_to_id для удаления категории");


        private final String message;
    }
}