package com.keepcode.keepcodeshop.exceptions;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class TokenException extends RuntimeException implements BaseException {

    private static final Integer TOKEN_EXCEPTION_CODE = -3;

    private final String errorMessage;

    public TokenException(ErrorMessage messageEnum) {
        this.errorMessage = messageEnum.getMessage();
    }

    @Override
    public Integer getCode() {
        return TOKEN_EXCEPTION_CODE;
    }

    @Getter
    @RequiredArgsConstructor
    public enum ErrorMessage {
        TOKEN_IS_EXPIRED_OR_INVALID("Токен истек или недействителен"),
        TOKEN_NOT_PRESENT("Не передан токен"),
        INVALID_TOKEN("Невалидный токен");

        private final String message;
    }
}
