package com.keepcode.keepcodeshop.services;

import com.keepcode.keepcodeshop.dto.entities.Product;
import com.keepcode.keepcodeshop.dto.views.product.ProductCreateView;
import com.keepcode.keepcodeshop.dto.views.product.ProductUpdateOrResponseView;
import com.keepcode.keepcodeshop.exceptions.RequestValidationException;

import java.util.List;

public interface ProductService {
    Product create(ProductCreateView view) throws RequestValidationException;
    Product update(ProductUpdateOrResponseView view) throws RequestValidationException;
    ProductUpdateOrResponseView findById(Long id);
    Product delete(Long rowId);
    List<ProductUpdateOrResponseView> getPage(Integer pageNum, Integer pageSize, Long categoryId);
    Product getNonNullById(Long id);
}
