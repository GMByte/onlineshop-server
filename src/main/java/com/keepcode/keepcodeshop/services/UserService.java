package com.keepcode.keepcodeshop.services;

import com.keepcode.keepcodeshop.dto.entities.User;
import com.keepcode.keepcodeshop.dto.views.auth.UserCreateView;
import com.keepcode.keepcodeshop.dto.views.user.UserPasswordChangeView;
import com.keepcode.keepcodeshop.dto.views.user.UserResponseView;
import com.keepcode.keepcodeshop.dto.views.user.UserRoleAssignView;
import com.keepcode.keepcodeshop.exceptions.RequestValidationException;

import java.util.List;

public interface UserService {

    void create(UserCreateView view) throws RequestValidationException;

    void changePassword(UserPasswordChangeView view, User user) throws RequestValidationException;

    void assignUserRole(UserRoleAssignView view) throws RequestValidationException;

    User getUserByUsername(String username);

    List<UserResponseView> list();

    User getNonNullById(Long id);
}
