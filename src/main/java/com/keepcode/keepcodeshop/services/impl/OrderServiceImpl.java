package com.keepcode.keepcodeshop.services.impl;

import com.keepcode.keepcodeshop.constants.enums.RowStatusCode;
import com.keepcode.keepcodeshop.dto.entities.Order;
import com.keepcode.keepcodeshop.dto.entities.ProductsInOrders;
import com.keepcode.keepcodeshop.dto.entities.User;
import com.keepcode.keepcodeshop.dto.views.order.OrderCreateView;
import com.keepcode.keepcodeshop.dto.views.order.OrderResponseView;
import com.keepcode.keepcodeshop.dto.views.order.OrderUpdateView;
import com.keepcode.keepcodeshop.dto.views.order.ProductToOrderView;
import com.keepcode.keepcodeshop.exceptions.ApiException;
import com.keepcode.keepcodeshop.exceptions.RequestValidationException;
import com.keepcode.keepcodeshop.repositories.OrderRepository;
import com.keepcode.keepcodeshop.repositories.ProductOrderRepository;
import com.keepcode.keepcodeshop.repositories.RowStatusRepository;
import com.keepcode.keepcodeshop.services.AbstractService;
import com.keepcode.keepcodeshop.services.ClientService;
import com.keepcode.keepcodeshop.services.OrderService;
import com.keepcode.keepcodeshop.services.ProductService;
import com.keepcode.keepcodeshop.utils.converters.ProductConverter;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.keepcode.keepcodeshop.constants.AppConfigConstants.ORDER_PREFIX;
import static com.keepcode.keepcodeshop.exceptions.RequestValidationException.ErrorMessage.ORDER_IS_BEING_PROCESSED;
import static com.keepcode.keepcodeshop.utils.TimeUtils.currentTimestamp;
import static java.lang.String.format;

@Service
public class OrderServiceImpl extends AbstractService<Order, OrderResponseView> implements OrderService {

    private static final String ORDER_NOT_FOUND = "Заказ с id:%s не найден";
    private static final String CLIENT_NOT_AVAILABLE = "Клиент по id: %s не найден или недоступен";
    private static final String ORDER_NOT_AVAILABLE = "Заказ не найден или недоступен";

    private final OrderRepository orderRepository;
    private final ProductService productService;
    private final ProductOrderRepository productOrderRepository;
    private final ClientService clientService;

    @Autowired
    public OrderServiceImpl(RowStatusRepository rowStatusRepository,
                            OrderRepository orderRepository,
                            ProductService productService, ProductOrderRepository productOrderRepository, ClientService clientService) {
        super(rowStatusRepository);
        this.orderRepository = orderRepository;
        this.productService = productService;
        this.productOrderRepository = productOrderRepository;
        this.clientService = clientService;
    }

    @Override
    @Transactional
    public Order create(OrderCreateView view, User user) {
        var order = new Order();
        var client = clientService.getNonNullById(view.getClientId());
        if (!user.getId().equals(client.getUserId())) {
            throw new ApiException(format(CLIENT_NOT_AVAILABLE, view.getClientId()));
        }
        order.setClient(client);
        order.setPriceTotal(0d);
        order.setRowStatus(getStatusIdByStatusCode(RowStatusCode.CREATED));
        order.setAddDate(currentTimestamp());
        order.setNumber(generateOrderNumber());
        final var mergedOrder = orderRepository.save(order);
        view.getItems().forEach(item -> saveItemAndSetTotal(item, mergedOrder));
        orderRepository.save(mergedOrder);
        return order;
    }

    @Override
    public void update(OrderUpdateView view) {
        var order = getNonNullById(view.getId());

        var clientId = view.getClientId();

        if (clientId != null) {
            order.setClient(clientService.getNonNullById(clientId));
        }

        var rowStatusCode = view.getRowStatusCode();

        if (rowStatusCode != null && !rowStatusCode.trim().isEmpty()) {
            order.setRowStatus(getStatusIdByStatusCode(rowStatusCode));
        }

        var postNumber = view.getPostNumber();

        if (postNumber != null) {
            order.setPostNumber(postNumber);
        }
        orderRepository.save(order);
    }

    @Override
    @Transactional
    public void addProductToOrder(ProductToOrderView view, User user) throws RequestValidationException {
        var order = getNonNullById(view.getOrderId());
        validateUserAndStatusUpdatable(order, user);
        var product = productService.getNonNullById(view.getProductId());
        var productInOrder = new ProductsInOrders();
        var orderProductKey = new ProductsInOrders.OrderProductKey(order, product);
        productInOrder.setOrderProductKey(orderProductKey);
        productInOrder.setProductAmount(view.getAmount());
        productOrderRepository.save(productInOrder);
        order.setPriceTotal(getPriceTotalForOrder(order) + product.getPrice());
        orderRepository.save(order);
    }

    @Override
    public List<OrderResponseView> listOrders(User user) {
        return orderRepository.findOrdersByUserIdAndNotInStatus(user.getId(), getStatusDeleted().getId()).stream()
                .map(this::toResponseView)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void deleteProductFromOrder(Long orderId, Long productId, User user) throws RequestValidationException {
        var order = getNonNullById(orderId);
        validateUserAndStatusUpdatable(order, user);
        var product = productService.getNonNullById(productId);
        var orderProductKey = new ProductsInOrders.OrderProductKey();
        orderProductKey.setOrder(order);
        orderProductKey.setProduct(product);
        productOrderRepository.deleteById(orderProductKey);
        var calculatedPriceTotal = getPriceTotalForOrder(order) - product.getPrice();
        if (calculatedPriceTotal < 0) {
            calculatedPriceTotal = 0;
        }
        order.setPriceTotal(calculatedPriceTotal);
        orderRepository.save(order);
    }

    @Override
    public void cancel(Long orderId, User user) throws RequestValidationException {
        var order = getNonNullById(orderId);
        validateUserAndStatusUpdatable(order, user);
        order.setRowStatus(getStatusDeleted());
        orderRepository.save(order);
    }

    @Override
    public void delete(Long orderId) {
        var order = getNonNullById(orderId);
        order.setRowStatus(getStatusDeleted());
        orderRepository.save(order);
    }

    @Override
    public List<OrderResponseView> findOrdersByUser(Long userId) {
        return orderRepository.findOrdersByUserIdAndNotInStatus(userId, getStatusDeleted().getId()).stream()
                .map(this::toResponseView)
                .collect(Collectors.toList());
    }

    @Override
    public List<OrderResponseView> findOrdersByClient(Long clientId) {
        return orderRepository.findOrdersByClientAndNotInStatus(clientId, getStatusDeleted().getId()).stream()
                .map(this::toResponseView)
                .collect(Collectors.toList());
    }

    @Override
    public Order getNonNullById(Long orderId) {
        return orderRepository.getOrderByIdAndRowStatusNot(orderId, getStatusDeleted())
                .orElseThrow(() -> new ApiException(format(ORDER_NOT_FOUND, orderId)));
    }

    private void saveItemAndSetTotal(OrderCreateView.Item item, Order order) {
        var product = productService.getNonNullById(item.getProductId());
        var orderProductKey = new ProductsInOrders.OrderProductKey(order, product);
        var productInOrder = new ProductsInOrders(orderProductKey, item.getAmount());
        var orderPriceTotal = getPriceTotalForOrder(order);
        order.setPriceTotal(orderPriceTotal + product.getPrice());
        productOrderRepository.save(productInOrder);
    }

    private String generateOrderNumber() {
        return ORDER_PREFIX + UUID.randomUUID();
    }

    private void validateUserAndStatusUpdatable(Order order, User user) throws RequestValidationException {
        if (order == null || !order.getClient().getUserId().equals(user.getId())) {
            throw new ApiException(ORDER_NOT_AVAILABLE);
        }
        if (!RowStatusCode.CREATED.name().equals(order.getRowStatus().getCode())) {
            throw new RequestValidationException(ORDER_IS_BEING_PROCESSED);
        }
    }

    private Double getPriceTotalForOrder(Order order) {
        return (order.getPriceTotal() != null)? order.getPriceTotal() : 0;
    }

    private List<OrderResponseView.ProductOnOrderView> getProductsForOrder(Order order) {
        return productOrderRepository.getProductsByOrderId(order.getId()).stream()
                .map(productsOnOrder -> {
                    var productOnOrderView = new OrderResponseView.ProductOnOrderView();
                    var product = ProductConverter.toResponseView(productsOnOrder.getOrderProductKey().getProduct());
                    productOnOrderView.setProduct(product);
                    productOnOrderView.setAmount(productsOnOrder.getProductAmount());
                    return productOnOrderView;
                }).collect(Collectors.toList());
    }

    @Override
    protected OrderResponseView toResponseView(Order entity) {
        return OrderResponseView.builder()
                .id(entity.getId())
                .clientId(entity.getClient().getId())
                .priceTotal(entity.getPriceTotal())
                .number(entity.getNumber())
                .addDate(entity.getAddDate())
                .postNumber(entity.getPostNumber())
                .rowStatusCode(entity.getRowStatus().getCode())
                .productsOnOrder(getProductsForOrder(entity))
                .build();
    }
}