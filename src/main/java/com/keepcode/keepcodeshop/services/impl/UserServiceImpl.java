package com.keepcode.keepcodeshop.services.impl;

import com.keepcode.keepcodeshop.constants.enums.Role;
import com.keepcode.keepcodeshop.dto.entities.User;
import com.keepcode.keepcodeshop.dto.views.auth.UserCreateView;
import com.keepcode.keepcodeshop.dto.views.user.UserPasswordChangeView;
import com.keepcode.keepcodeshop.dto.views.user.UserResponseView;
import com.keepcode.keepcodeshop.dto.views.user.UserRoleAssignView;
import com.keepcode.keepcodeshop.exceptions.ApiException;
import com.keepcode.keepcodeshop.exceptions.RequestValidationException;
import com.keepcode.keepcodeshop.repositories.RowStatusRepository;
import com.keepcode.keepcodeshop.repositories.UserRepository;
import com.keepcode.keepcodeshop.services.AbstractService;
import com.keepcode.keepcodeshop.services.UserService;
import lombok.var;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.keepcode.keepcodeshop.constants.AppConfigConstants.EMAIL_REGEX_FORMAT;
import static com.keepcode.keepcodeshop.constants.AppConfigConstants.PASSWORD_REGEX_FORMAT;
import static com.keepcode.keepcodeshop.exceptions.RequestValidationException.ErrorMessage.EMAIL_ALREADY_REGISTERED;
import static com.keepcode.keepcodeshop.exceptions.RequestValidationException.ErrorMessage.INVALID_EMAIL_FORMAT;
import static com.keepcode.keepcodeshop.exceptions.RequestValidationException.ErrorMessage.INVALID_PASSWORD_FORMAT;
import static com.keepcode.keepcodeshop.exceptions.RequestValidationException.ErrorMessage.NEW_PASSWORD_MATCHES_OLD;
import static com.keepcode.keepcodeshop.exceptions.RequestValidationException.ErrorMessage.OLD_PASSWORD_WRONG;
import static com.keepcode.keepcodeshop.exceptions.RequestValidationException.ErrorMessage.PASSWORDS_CONFIRMATION_FAILED;
import static com.keepcode.keepcodeshop.exceptions.RequestValidationException.ErrorMessage.WRONG_ROLE_SPECIFIED;
import static com.keepcode.keepcodeshop.utils.TimeUtils.currentTimestamp;
import static java.util.Objects.nonNull;
import static java.util.regex.Pattern.compile;

@Service
public class UserServiceImpl extends AbstractService<User, UserResponseView> implements UserService {

    private static final String USER_NOT_FOUND_BY_ID = "Пользователь не найден по id: ";
    private static final Pattern EMAIL_REGEX = compile(EMAIL_REGEX_FORMAT);
    private static final Pattern PASSWORD_REGEX = compile(PASSWORD_REGEX_FORMAT);

    private final UserRepository userRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserServiceImpl(RowStatusRepository rowStatusRepository, UserRepository userRepository,
                           BCryptPasswordEncoder bCryptPasswordEncoder) {
        super(rowStatusRepository);
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public List<UserResponseView> list() {
        return userRepository.findAll().stream()
                .map(this::toResponseView)
                .collect(Collectors.toList());
    }

    @Override
    public void create(UserCreateView view) throws RequestValidationException {
        validate(view);

        var user = new User();

        user.setName(view.getEmail());
        user.setPassword(bCryptPasswordEncoder.encode(view.getPassword()));
        user.setRole(Role.USER.name());
        user.setAddDate(currentTimestamp());

        userRepository.save(user);
    }

    @Override
    public void changePassword(UserPasswordChangeView view, User user) throws RequestValidationException {
        validate(view, user);
        user.setPassword(bCryptPasswordEncoder.encode(view.getNewPassword()));
        userRepository.save(user);
    }

    @Override
    public void assignUserRole(UserRoleAssignView view) throws RequestValidationException {
        var role = view.getRole();
        validateRole(role);
        var user = userRepository.findById(view.getUserId())
                .orElseThrow(() -> new ApiException(USER_NOT_FOUND_BY_ID + view.getUserId()));
        user.setRole(role);
        userRepository.save(user);
    }

    private void validateRole(String role) throws RequestValidationException {
        try {
            Role.valueOf(role);
        } catch (IllegalArgumentException exception) {
            throw new RequestValidationException(WRONG_ROLE_SPECIFIED);
        }
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.getUserByName(username);
    }

    @Override
    public User getNonNullById(Long id) throws ApiException {
        return userRepository.findById(id)
                .orElseThrow(() -> new ApiException(USER_NOT_FOUND_BY_ID + id));
    }

    private void validate(UserCreateView view) throws RequestValidationException {
        var sameEmailUser = getUserByUsername(view.getEmail());

        if (nonNull(sameEmailUser)) {
            throw new RequestValidationException(EMAIL_ALREADY_REGISTERED);
        }
        validateEmail(view.getEmail());
        validatePassword(view.getPassword());
    }

    private void validate(UserPasswordChangeView view, User user) throws RequestValidationException {
        if (!bCryptPasswordEncoder.matches(view.getOldPassword(), user.getPassword())) {
            throw new RequestValidationException(OLD_PASSWORD_WRONG);
        }
        var newPassword = view.getNewPassword();
        if (newPassword.equals(view.getOldPassword())) {
            throw new RequestValidationException(NEW_PASSWORD_MATCHES_OLD);
        }
        if (!newPassword.equals(view.getConfirmNewPassword())) {
            throw new RequestValidationException(PASSWORDS_CONFIRMATION_FAILED);
        }
        validatePassword(newPassword);
    }

    private void validateEmail(String email) throws RequestValidationException {
       if (!EMAIL_REGEX.matcher(email).matches()) {
           throw new RequestValidationException(INVALID_EMAIL_FORMAT);
       }
    }

    private void validatePassword(String password) throws RequestValidationException {
        if (!PASSWORD_REGEX.matcher(password).matches()) {
            throw new RequestValidationException(INVALID_PASSWORD_FORMAT);
        }
    }

    @Override
    protected UserResponseView toResponseView(User entity) {
        return UserResponseView.builder()
                .id(entity.getId())
                .username(entity.getName())
                .role(entity.getRole().name())
                .addDate(entity.getAddDate())
                .lastLoginDate(entity.getLastLoginDate())
                .build();
    }
}