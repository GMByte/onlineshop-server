package com.keepcode.keepcodeshop.services.impl;

import com.keepcode.keepcodeshop.constants.enums.RowStatusCode;
import com.keepcode.keepcodeshop.dto.entities.Category;
import com.keepcode.keepcodeshop.dto.entities.Product;
import com.keepcode.keepcodeshop.dto.views.category.CategoryCreateView;
import com.keepcode.keepcodeshop.dto.views.category.CategoryUpdateOrResponseView;
import com.keepcode.keepcodeshop.exceptions.ApiException;
import com.keepcode.keepcodeshop.repositories.CategoryRepository;
import com.keepcode.keepcodeshop.repositories.ProductRepository;
import com.keepcode.keepcodeshop.repositories.RowStatusRepository;
import com.keepcode.keepcodeshop.services.AbstractService;
import com.keepcode.keepcodeshop.services.CategoryService;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.keepcode.keepcodeshop.exceptions.ApiException.ErrorMessage.MOVE_TO_ID_NOT_PROVIDED;
import static java.lang.String.format;
import static java.util.Objects.nonNull;

@Service
public class CategoryServiceImpl extends AbstractService<Category, CategoryUpdateOrResponseView> implements CategoryService {

    private static final String CATEGORY_NOT_FOUND = "Категория с id:%s не найдена";

    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;

    @Autowired
    public CategoryServiceImpl(RowStatusRepository rowStatusRepository,
                               CategoryRepository categoryRepository,
                               ProductRepository productRepository) {
        super(rowStatusRepository);
        this.categoryRepository = categoryRepository;
        this.productRepository = productRepository;
    }


    public Category create(CategoryCreateView view) {
        var category = new Category(view.getName());
        return categoryRepository.save(category);
    }

    public Category update(CategoryUpdateOrResponseView view) {
        var category = getNonNullById(view.getId());
        category.setName(view.getName());
        return categoryRepository.save(category);
    }

    @Override
    @Transactional
    public void delete(Long id, Long moveToId) {
        Category category = getNonNullById(id);
        List<Product> productsBoundToCategory = productRepository.findProductsByCategory(category);
        if (!productsBoundToCategory.isEmpty()) {
            changeProductCategoryOrThrow(productsBoundToCategory, moveToId);
        }
        categoryRepository.deleteById(id);
    }

    @Override
    public CategoryUpdateOrResponseView findById(Long id) {
        return toResponseView(getNonNullById(id));
    }

    @Override
    public List<CategoryUpdateOrResponseView> listCategories() {
        var categoryIterable = categoryRepository.findAll();
        return StreamSupport.stream(categoryIterable.spliterator(), false)
                .map(this::toResponseView)
                .collect(Collectors.toList());
    }

    private void changeProductCategoryOrThrow(List<Product> products, Long moveToId) throws ApiException {
        if (moveToId == null && containsNonDeletedProducts(products)) {
            throw new ApiException(format(MOVE_TO_ID_NOT_PROVIDED.getMessage(), getCountOfNonDeletedProducts(products)));
        }
        products.forEach(product -> product.setCategory(null));
        if (nonNull(moveToId)) {
            var categoryToMoveTo = getNonNullById(moveToId);
            products.forEach(product -> product.setCategory(categoryToMoveTo));
        }
        productRepository.saveAll(products);
    }

    private boolean containsNonDeletedProducts(List<Product> products) {
        return !products.stream().allMatch(this::isProductDeleted);
    }

    private boolean isProductDeleted(Product product) {
        return RowStatusCode.DELETED.equals(product.getRowStatus().toRowStatusCode());
    }

    private Long getCountOfNonDeletedProducts(List<Product> products) {
        return products.stream().filter(product -> !isProductDeleted(product)).count();
    }

    @Override
    public Category getNonNullById(Long id) throws ApiException {
        return categoryRepository.findById(id)
                .orElseThrow(() -> new ApiException(format(CATEGORY_NOT_FOUND, id)));
    }

    protected CategoryUpdateOrResponseView toResponseView(Category category) {
        return new CategoryUpdateOrResponseView(category.getId(), category.getName());
    }
}