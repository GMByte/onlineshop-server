package com.keepcode.keepcodeshop.services.impl;

import com.keepcode.keepcodeshop.constants.enums.RowStatusCode;
import com.keepcode.keepcodeshop.dto.entities.Client;
import com.keepcode.keepcodeshop.dto.entities.User;
import com.keepcode.keepcodeshop.dto.views.client.ClientCreateView;
import com.keepcode.keepcodeshop.dto.views.client.ClientUpdateOrResponseView;
import com.keepcode.keepcodeshop.exceptions.ApiException;
import com.keepcode.keepcodeshop.exceptions.RequestValidationException;
import com.keepcode.keepcodeshop.repositories.ClientRepository;
import com.keepcode.keepcodeshop.repositories.RowStatusRepository;
import com.keepcode.keepcodeshop.services.AbstractService;
import com.keepcode.keepcodeshop.services.ClientService;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.keepcode.keepcodeshop.constants.AppConfigConstants.MOBILE_REGEX_FORMAT;
import static com.keepcode.keepcodeshop.exceptions.RequestValidationException.ErrorMessage.INVALID_PHONE_FORMAT;
import static com.keepcode.keepcodeshop.utils.TimeUtils.currentTimestamp;
import static java.util.regex.Pattern.compile;

@Service
@Slf4j
public class ClientServiceImpl extends AbstractService<Client, ClientUpdateOrResponseView> implements ClientService {

    private static final String CLIENT_NOT_AVAILABLE = "Клиент не найден или не доступен";
    private static final String UNABLE_TO_UPDATE_CLIENT_FOR_USER = "Не удалось обновить клиент id: {} для пользователя id: {}";
    private static final String CLIENT_NOT_FOUND_BY_ID = "Не найден клиент по id: ";
    private static final Pattern MOBILE_REGEX = compile(MOBILE_REGEX_FORMAT);

    private final ClientRepository clientRepository;

    @Autowired
    public ClientServiceImpl(RowStatusRepository rowStatusRepository,
                             ClientRepository clientRepository) {
        super(rowStatusRepository);
        this.clientRepository = clientRepository;
    }

    @Override
    public Client create(ClientCreateView view, User user) throws RequestValidationException {
        validatePhone(view.getContactPhone());
        var client = new Client();
        client.setUserId(user.getId());
        client.setFullName(view.getFullName());
        client.setContactPhone(view.getContactPhone());
        client.setRowStatus(getStatusIdByStatusCode(RowStatusCode.ACTIVE));
        client.setAddDate(currentTimestamp());
        return clientRepository.save(client);
    }

    @Override
    public void update(ClientUpdateOrResponseView view, User user) throws RequestValidationException {
        var client = getClientForUser(view.getId(), user);

        var fullName = view.getFullName();

        if (fullName != null) {
            client.setFullName(fullName);
        }

        var contactPhone = view.getContactPhone();

        if (contactPhone != null) {
            validatePhone(contactPhone);
            client.setContactPhone(contactPhone);
        }
        clientRepository.save(client);
    }

    public void delete(Long clientId, User user) {
        Client client = getClientForUser(clientId, user);
        client.setRowStatus(getStatusDeleted());
        clientRepository.save(client);
    }

    @Override
    public List<ClientUpdateOrResponseView> listUserClients(User user) {
        return clientRepository.getClientsByUserIdAndRowStatusNot(user.getId(), getStatusDeleted()).stream()
                .map(this::toResponseView)
                .collect(Collectors.toList());
    }

    @Override
    public List<ClientUpdateOrResponseView> listClients() {
        var clientIterable = clientRepository.getClientsByRowStatusNot(getStatusDeleted());
        return clientIterable.stream()
                .map(this::toResponseView)
                .collect(Collectors.toList());
    }

    @Override
    public Client getNonNullById(Long id) throws ApiException {
        return clientRepository.getClientByIdAndRowStatusNot(id, getStatusDeleted())
                .orElseThrow(() -> new ApiException(CLIENT_NOT_FOUND_BY_ID + id));
    }

    private Client getClientForUser(Long clientId, User user) {
        try {
            var client = getNonNullById(clientId);
            if (!client.getUserId().equals(user.getId())) {
                throw new IllegalArgumentException();
            }
            return client;
        } catch (IllegalArgumentException exception) {
            log.error(UNABLE_TO_UPDATE_CLIENT_FOR_USER, clientId, user.getId());
            throw new ApiException(CLIENT_NOT_AVAILABLE);
        }
    }

    private void validatePhone(String phoneNumber) throws RequestValidationException {
        if (!MOBILE_REGEX.matcher(phoneNumber).matches()) {
            throw new RequestValidationException(INVALID_PHONE_FORMAT);
        }
    }

    @Override
    protected ClientUpdateOrResponseView toResponseView(Client entity) {
        return ClientUpdateOrResponseView.builder()
                .id(entity.getId())
                .userId(entity.getUserId())
                .fullName(entity.getFullName())
                .contactPhone(entity.getContactPhone())
                .addDate(entity.getAddDate())
                .build();
    }
}
