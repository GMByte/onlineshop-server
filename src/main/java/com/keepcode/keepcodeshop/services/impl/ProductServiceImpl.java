package com.keepcode.keepcodeshop.services.impl;

import com.keepcode.keepcodeshop.constants.enums.RowStatusCode;
import com.keepcode.keepcodeshop.dto.entities.Product;
import com.keepcode.keepcodeshop.dto.views.product.ProductCreateView;
import com.keepcode.keepcodeshop.dto.views.product.ProductUpdateOrResponseView;
import com.keepcode.keepcodeshop.exceptions.ApiException;
import com.keepcode.keepcodeshop.exceptions.RequestValidationException;
import com.keepcode.keepcodeshop.repositories.ProductRepository;
import com.keepcode.keepcodeshop.repositories.RowStatusRepository;
import com.keepcode.keepcodeshop.services.AbstractService;
import com.keepcode.keepcodeshop.services.CategoryService;
import com.keepcode.keepcodeshop.services.ProductService;
import com.keepcode.keepcodeshop.utils.converters.ProductConverter;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.keepcode.keepcodeshop.constants.enums.RowStatusCode.ACTIVE;
import static com.keepcode.keepcodeshop.exceptions.RequestValidationException.ErrorMessage.NEGATIVE_PRICE_VALUE;
import static java.lang.String.format;

@Slf4j
@Service
public class ProductServiceImpl extends AbstractService<Product, ProductUpdateOrResponseView> implements ProductService {

    private static final String PRODUCT_NOT_FOUND_BY_ID = "Товар с id:%s не найден";

    private final ProductRepository productRepository;
    private final CategoryService categoryService;

    @Autowired
    public ProductServiceImpl(RowStatusRepository rowStatusRepository, ProductRepository productRepository,
                              CategoryService categoryService) {
        super(rowStatusRepository);
        this.productRepository = productRepository;
        this.categoryService = categoryService;
    }

    @Override
    public Product create(ProductCreateView view) throws RequestValidationException {
        var product = new Product();

        product.setName(view.getName());
        product.setBalanceUnits(view.getBalance());
        product.setMeasurementUnits(view.getMeasurementUnits());

        var price = view.getPrice();

        validatePrice(price);

        product.setPrice(price);

        var category = categoryService.getNonNullById(view.getCategoryId());
        product.setCategory(category);

        var activeRowStatus = getStatusIdByStatusCode(ACTIVE);
        product.setRowStatus(activeRowStatus);

        return productRepository.save(product);
    }

    @Override
    public Product update(ProductUpdateOrResponseView view) throws RequestValidationException {
        var product = getNonNullById(view.getId());

        if (view.getName() != null) {
            product.setName(view.getName());
        }
        if (view.getBalance() != null) {
            product.setBalanceUnits(view.getBalance());
        }
        if (view.getMeasurementUnits() != null) {
            product.setMeasurementUnits(view.getMeasurementUnits());
        }
        if (view.getCategoryId() != null) {
            var category = categoryService.getNonNullById(view.getCategoryId());
            product.setCategory(category);
        }
        if (view.getPrice() != null) {
            validatePrice(view.getPrice());
            product.setPrice(view.getPrice());
        }
        return productRepository.save(product);
    }

    @Override
    public Product delete(Long rowId) {
        var product = getNonNullById(rowId);
        var rowStatusDeleted = getStatusIdByStatusCode(RowStatusCode.DELETED);
        product.setRowStatus(rowStatusDeleted);
        return productRepository.save(product);
    }

    @Override
    public ProductUpdateOrResponseView findById(Long id) {
        return toResponseView(getNonNullById(id));
    }

    @Override
    public List<ProductUpdateOrResponseView> getPage(Integer pageNum, Integer pageSize, Long categoryId) {
        var activeRowStatus = getStatusIdByStatusCode(ACTIVE);
        var productPage = productRepository.findProductsByCategoryOptional(categoryId, activeRowStatus, PageRequest.of(pageNum - 1, pageSize));
        return productPage.get()
                .map(this::toResponseView)
                .collect(Collectors.toList());
    }

    @Override
    public Product getNonNullById(Long productId) {
        return productRepository.findByIdAndAndRowStatusNot(productId, getStatusDeleted())
                .orElseThrow(() -> new ApiException(format(PRODUCT_NOT_FOUND_BY_ID, productId)));
    }

    private void validatePrice(Double price) throws RequestValidationException {
        if (price < 0) {
            throw new RequestValidationException(NEGATIVE_PRICE_VALUE);
        }
    }

    @Override
    protected ProductUpdateOrResponseView toResponseView(Product product) {
        return ProductConverter.toResponseView(product);
    }
}