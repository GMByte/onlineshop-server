package com.keepcode.keepcodeshop.services;

import com.keepcode.keepcodeshop.dto.entities.Category;
import com.keepcode.keepcodeshop.dto.views.category.CategoryCreateView;
import com.keepcode.keepcodeshop.dto.views.category.CategoryUpdateOrResponseView;

import java.util.List;

public interface CategoryService {
    Category create(CategoryCreateView view);
    Category update(CategoryUpdateOrResponseView view);
    void delete(Long id, Long moveToId);
    CategoryUpdateOrResponseView findById(Long id);
    List<CategoryUpdateOrResponseView> listCategories();
    Category getNonNullById(Long id);
}
