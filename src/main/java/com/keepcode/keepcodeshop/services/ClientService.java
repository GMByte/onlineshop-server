package com.keepcode.keepcodeshop.services;

import com.keepcode.keepcodeshop.dto.entities.Client;
import com.keepcode.keepcodeshop.dto.entities.User;
import com.keepcode.keepcodeshop.dto.views.client.ClientCreateView;
import com.keepcode.keepcodeshop.dto.views.client.ClientUpdateOrResponseView;
import com.keepcode.keepcodeshop.exceptions.RequestValidationException;

import java.util.List;

public interface ClientService {
    Client create(ClientCreateView view, User user) throws RequestValidationException;

    List<ClientUpdateOrResponseView> listClients();

    void update(ClientUpdateOrResponseView view, User user) throws RequestValidationException;

    List<ClientUpdateOrResponseView> listUserClients(User user);

    void delete(Long clientId, User user);

    Client getNonNullById(Long id);
}