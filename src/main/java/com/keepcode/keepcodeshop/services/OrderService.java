package com.keepcode.keepcodeshop.services;

import com.keepcode.keepcodeshop.dto.entities.Order;
import com.keepcode.keepcodeshop.dto.entities.User;
import com.keepcode.keepcodeshop.dto.views.order.OrderCreateView;
import com.keepcode.keepcodeshop.dto.views.order.OrderResponseView;
import com.keepcode.keepcodeshop.dto.views.order.OrderUpdateView;
import com.keepcode.keepcodeshop.dto.views.order.ProductToOrderView;
import com.keepcode.keepcodeshop.exceptions.ApiException;
import com.keepcode.keepcodeshop.exceptions.RequestValidationException;

import java.util.List;

public interface OrderService {
    Order create(OrderCreateView view, User user);

    void update(OrderUpdateView view);

    void addProductToOrder(ProductToOrderView view, User user) throws RequestValidationException;

    List<OrderResponseView> listOrders(User user);

    void deleteProductFromOrder(Long orderId, Long productId, User user) throws RequestValidationException;

    void cancel(Long orderId, User user) throws RequestValidationException;

    void delete(Long orderId);

    List<OrderResponseView> findOrdersByUser(Long userId);

    List<OrderResponseView> findOrdersByClient(Long clientId);

    Order getNonNullById(Long id) throws ApiException;
}
