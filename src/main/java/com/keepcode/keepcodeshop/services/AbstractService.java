package com.keepcode.keepcodeshop.services;

import com.keepcode.keepcodeshop.constants.enums.RowStatusCode;
import com.keepcode.keepcodeshop.dto.entities.RowStatus;
import com.keepcode.keepcodeshop.exceptions.ApiException;
import com.keepcode.keepcodeshop.repositories.RowStatusRepository;
import lombok.RequiredArgsConstructor;

import static java.lang.String.format;

@RequiredArgsConstructor
public abstract class AbstractService<E, V> {

    private final RowStatusRepository rowStatusRepository;

    protected abstract V toResponseView(E entity);
    protected abstract E getNonNullById(Long id) throws ApiException;

    protected RowStatus getStatusIdByStatusCode(RowStatusCode rowStatusCode) {
        return getStatusIdByStatusCode(rowStatusCode.name());
    }

    protected RowStatus getStatusIdByStatusCode(String rowStatusCode) {
        return rowStatusRepository.getByCode(rowStatusCode)
                .orElseThrow(() -> new ApiException(format("Не найден статус по коду: %s", rowStatusCode)));
    }

    protected RowStatus getStatusDeleted() {
       return getStatusIdByStatusCode(RowStatusCode.DELETED);
    }
}
