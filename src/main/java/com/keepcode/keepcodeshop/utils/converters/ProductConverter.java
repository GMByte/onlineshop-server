package com.keepcode.keepcodeshop.utils.converters;

import com.keepcode.keepcodeshop.dto.entities.Product;
import com.keepcode.keepcodeshop.dto.views.product.ProductUpdateOrResponseView;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.var;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductConverter {
    public static ProductUpdateOrResponseView toResponseView(Product product) {
        var productResponseView = new ProductUpdateOrResponseView();
        productResponseView.setId(product.getId());
        productResponseView.setName(product.getName());
        productResponseView.setBalance(product.getBalanceUnits());
        productResponseView.setMeasurementUnits(product.getMeasurementUnits());
        productResponseView.setCategoryId(product.getCategory() != null ? product.getCategory().getId() : null);
        productResponseView.setPrice(product.getPrice());
        return productResponseView;
    }
}