package com.keepcode.keepcodeshop.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static com.keepcode.keepcodeshop.constants.AppConfigConstants.BEARER_TOKEN_PREFIX;
import static org.apache.logging.log4j.util.Strings.isNotBlank;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TokenUtils {
    public static boolean isValidBearerTokenHeaderValue(String tokenHeaderValue) {
        return isNotBlank(tokenHeaderValue) && tokenHeaderValue.startsWith(BEARER_TOKEN_PREFIX);
    }

    public static String getBearerTokenValue(String tokenHeaderValue) {
        return tokenHeaderValue.replace(BEARER_TOKEN_PREFIX + " ", "");
    }
}
