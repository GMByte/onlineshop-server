package com.keepcode.keepcodeshop.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.keepcode.keepcodeshop.dto.views.BaseResponse;

import com.keepcode.keepcodeshop.dto.views.RowIdView;
import com.keepcode.keepcodeshop.exceptions.BaseException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.var;

import static com.keepcode.keepcodeshop.constants.enums.ResponseCode.STATUS_OK;
import static java.util.Objects.requireNonNull;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ResponseUtils {

    public static BaseResponse respondOkWithId(Long rowId, String traceId) {
        var rowIdView = new RowIdView(rowId);
        return respondOkWithBody(rowIdView, traceId);
    }

    public static BaseResponse respondOk(String traceId) {
        return getOkResponse(traceId);
    }

    public static BaseResponse respondError(BaseException baseException, String traceId) {
        var response = new BaseResponse();
        var header = new BaseResponse.ResponseHeader();
        header.setTraceId(traceId);
        header.setStatusCode(baseException.getCode());
        header.setMessage(baseException.getErrorMessage());
        response.setHeader(header);
        return response;
    }

    private static BaseResponse getOkResponse(String traceId) {
        var response = new BaseResponse();
        var header = new BaseResponse.ResponseHeader();
        header.setTraceId(traceId);
        header.setStatusCode(STATUS_OK.getCode());
        header.setMessage(STATUS_OK.getTextMessage());
        response.setHeader(header);
        return response;
    }

    public static BaseResponse respondOkWithBody(Object body, String traceId) {
        var response = getOkResponse(traceId);
        response.setBody(body);
        return response;
    }

    public static String convertObjectToJson(Object object) throws JsonProcessingException {
        requireNonNull(object);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(object);
    }
}
