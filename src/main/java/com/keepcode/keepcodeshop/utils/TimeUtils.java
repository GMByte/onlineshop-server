package com.keepcode.keepcodeshop.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Date;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TimeUtils {
    public static Long minutesToMills(Long minutes) {
        return minutes * 60000;
    }

    public static Timestamp currentTimestamp() {
        return new Timestamp(new Date().getTime());
    }
}