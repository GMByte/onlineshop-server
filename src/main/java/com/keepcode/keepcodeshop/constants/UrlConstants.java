package com.keepcode.keepcodeshop.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UrlConstants {
    /*
     * Префиксы операций
     */
    public static final String WRITE_OPERATION_PREFIX = "/update";
    public static final String READ_OPERATION_PREFIX = "/data";

    /*
     * Базовые пути контроллеров
     */
    public static final String USER_BASE_PATH = "/users";
    public static final String PRODUCT_BASE_PATH = "/products";
    public static final String CLIENT_BASE_PATH = "/clients";
    public static final String ORDER_BASE_PATH = "/orders";
    public static final String CATEGORY_BASE_PATH = "/categories";

    /*
    * Стандартные URL для операций CRUD
    */
    public static final String GET_ALL_REQUEST_PATH =  "/all";
    public static final String CREATE_REQUEST_PATH = "/create";
    public static final String UPDATE_REQUEST_PATH = "/update";
    public static final String DELETE_REQUEST_PATH = "/delete";
    public static final String GET_BY_ID_REQUEST_PATH = "/{id}";

    /*
     * Matchers
     */
    public static final String BASE_PRODUCTS_URL_MARCHER = "**" + PRODUCT_BASE_PATH + "/**";
    public static final String BASE_CATEGORIES_URL_MATCHER = "**" + CATEGORY_BASE_PATH + "/**";
    public static final String BASE_CLIENTS_URL_MATCHER = "**" + CLIENT_BASE_PATH + "/**";
    public static final String BASE_ORDERS_URL_MATCHER = "**" + ORDER_BASE_PATH + "/**";
    public static final String BASE_USERS_URL_MATCHER = "**" + USER_BASE_PATH + "/**";
}
