package com.keepcode.keepcodeshop.constants.enums;

public enum Permission {
    PRODUCTS_READ("products:read"),
    PRODUCTS_MANAGE("products:manage"),
    USERS_READ("users:read"),
    USERS_WRITE("users:write"),
    CLIENTS_READ("clients:read");

    private final String authority;

    Permission(String authority) {
        this.authority = authority;
    }

    public String getAuthority() {
        return authority;
    }
}
