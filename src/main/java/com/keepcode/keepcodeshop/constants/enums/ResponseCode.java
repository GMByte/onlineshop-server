package com.keepcode.keepcodeshop.constants.enums;

import lombok.Getter;

@Getter
public enum ResponseCode {

    STATUS_OK(0, "Запрос обработан успешно");

    private final Integer code;
    private final String textMessage;

    ResponseCode(Integer code, String textMessage) {
        this.code = code;
        this.textMessage = textMessage;
    }
}