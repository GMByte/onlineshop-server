package com.keepcode.keepcodeshop.constants.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum RowStatusCode {
    ACTIVE,
    DELETED,
    CREATED,
    MANUAL,
    PUBLISHED
}