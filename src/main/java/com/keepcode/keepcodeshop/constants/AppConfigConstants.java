package com.keepcode.keepcodeshop.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AppConfigConstants {
    public static final Integer DEFAULT_PAGE_SIZE = 10;
    public static final String ORDER_PREFIX = "KCS-";
    public static final String BEARER_TOKEN_PREFIX = "Bearer";

    /*
     * Regex
     */
    public static final String MOBILE_REGEX_FORMAT = "^\\+?[78][-(]?\\d{3}\\)?-?\\d{3}-?\\d{2}-?\\d{2}$";
    public static final String EMAIL_REGEX_FORMAT = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
    public static final String PASSWORD_REGEX_FORMAT = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
}