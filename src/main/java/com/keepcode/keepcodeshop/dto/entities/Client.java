package com.keepcode.keepcodeshop.dto.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "clients")
@Data
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "clnt_id")
    private Long id;

    @Column(name = "usr_id")
    private Long userId;

    @Column(name = "clnt_full_name")
    private String fullName;

    @Column(name = "clnt_contact_phone")
    private String contactPhone;

    @Column(name = "clnt_add_date")
    private Timestamp addDate;

    @ManyToOne
    @JoinColumn(name = "rsts_id", referencedColumnName = "rsts_id")
    private RowStatus rowStatus;
}
