package com.keepcode.keepcodeshop.dto.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "products_to_orders")
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductsInOrders {

    @EmbeddedId
    private OrderProductKey orderProductKey;

    @Column(name = "prdt_ordr_amount")
    private Double productAmount;

    @Embeddable
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class OrderProductKey implements Serializable {
        @ManyToOne
        @JoinColumn(name = "ordr_id", referencedColumnName = "ordr_id")
        private Order order;

        @ManyToOne
        @JoinColumn(name = "prdt_id", referencedColumnName = "prdt_id")
        private Product product;
    }
}
