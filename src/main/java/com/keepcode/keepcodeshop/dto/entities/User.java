package com.keepcode.keepcodeshop.dto.entities;

import com.keepcode.keepcodeshop.constants.enums.Role;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "users")
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "usr_id")
    private Long id;

    @Column(name = "usr_name")
    private String name;

    @Column(name = "usr_password")
    private String password;

    @Column(name = "usr_role")
    private String role;

    @Column(name = "usr_add_date")
    private Timestamp addDate;

    @Column(name = "usr_last_login_date")
    private Timestamp lastLoginDate;

    public Role getRole() {
        return Role.valueOf(role);
    }
}
