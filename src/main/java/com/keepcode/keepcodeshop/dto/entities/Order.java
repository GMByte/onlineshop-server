package com.keepcode.keepcodeshop.dto.entities;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "orders")
@Data
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ordr_id")
    private Long id;

    @OneToOne
    @JoinColumn(name = "clnt_id", referencedColumnName = "clnt_id")
    private Client client;

    @Column(name = "ordr_price_total")
    private Double priceTotal;

    @Column(name = "ordr_add_date")
    private Timestamp addDate;

    @Column(name = "ordr_number")
    private String number;

    @ManyToOne
    @JoinColumn(name = "rsts_id", referencedColumnName = "rsts_id")
    private RowStatus rowStatus;

    @Column(name = "ordr_post_number")
    private String postNumber;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "producs_to_orders",
            joinColumns = { @JoinColumn(name = "ordr_id") },
            inverseJoinColumns = { @JoinColumn(name = "prdt_id") }
    )
    private List<Product> products = new ArrayList<>();
}
