package com.keepcode.keepcodeshop.dto.entities;

import com.keepcode.keepcodeshop.constants.enums.RowStatusCode;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "row_statuses")
@Data
@NoArgsConstructor
public class RowStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rsts_id")
    private Long id;

    @Column(name = "rsts_code")
    private String code;

    public RowStatus(String code) {
        this.code = code;
    }

    public RowStatusCode toRowStatusCode() {
       return RowStatusCode.valueOf(code);
    }
}
