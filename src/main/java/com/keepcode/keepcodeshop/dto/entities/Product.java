package com.keepcode.keepcodeshop.dto.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "products")
@Data
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "prdt_id")
    private Long id;

    @Column(name = "prdt_name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "ctgr_id", referencedColumnName = "ctgr_id")
    private Category category;

    @Column(name = "prdt_balance_units")
    private Double balanceUnits;

    @Column(name = "prdt_measurement_units")
    private String measurementUnits;

    @Column(name = "prdt_price")
    private Double price;

    @ManyToOne
    @JoinColumn(name = "rsts_id", referencedColumnName = "rsts_id")
    private RowStatus rowStatus;

    @ManyToMany(mappedBy = "products")
    private List<Order> orders = new ArrayList<>();
}
