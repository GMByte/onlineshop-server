package com.keepcode.keepcodeshop.dto.views.product;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter @Setter
public class ProductCreateView {

    private String name;
    private Double balance;
    private String measurementUnits;
    private Long categoryId;
    private Double price;

    @JsonCreator
    public ProductCreateView(@JsonProperty(value = "name", required = true) String name,
                             @JsonProperty(value = "balance", required = true) Double balance,
                             @JsonProperty(value = "measurement_units", required = true) String measurementUnits,
                             @JsonProperty(value = "category_id", required = true) Long categoryId,
                             @JsonProperty(value = "price", required = true) Double price) {
        this.name = name;
        this.balance = balance;
        this.measurementUnits = measurementUnits;
        this.categoryId = categoryId;
        this.price = price;
    }
}