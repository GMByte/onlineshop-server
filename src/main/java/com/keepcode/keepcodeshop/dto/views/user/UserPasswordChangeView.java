package com.keepcode.keepcodeshop.dto.views.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UserPasswordChangeView {

    private String oldPassword;

    private String newPassword;

    private String confirmNewPassword;

    @JsonCreator
    public UserPasswordChangeView(@JsonProperty(value = "old_password", required = true) String oldPassword,
                              @JsonProperty(value = "new_password", required = true) String newPassword,
                                 @JsonProperty(value = "confirm_new_password", required = true) String confirmNewPassword) {
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
        this.confirmNewPassword = confirmNewPassword;
    }
}
