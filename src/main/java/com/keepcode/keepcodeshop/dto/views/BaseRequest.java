package com.keepcode.keepcodeshop.dto.views;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class BaseRequest <T> {

    private T body;
    private RequestHeader header;

    @JsonCreator
    public BaseRequest(@JsonProperty(value = "body") T body,
                       @JsonProperty(value = "header") RequestHeader header) {
        this.body = body;
        this.header = header;
    }

    @Getter @Setter
    @NoArgsConstructor
    public static class RequestHeader {
        private String user;

        @JsonCreator
        public RequestHeader(@JsonProperty(value = "user") String user) {
            this.user = user;
        }
    }
}
