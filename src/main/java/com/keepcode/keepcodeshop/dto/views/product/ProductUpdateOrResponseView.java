package com.keepcode.keepcodeshop.dto.views.product;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Getter @Setter
public class ProductUpdateOrResponseView {

    private Long id;
    private String name;
    private Double balance;
    private String measurementUnits;
    private Long categoryId;
    private Double price;

    @JsonCreator
    public ProductUpdateOrResponseView(@JsonProperty(value = "id", required = true) Long id,
                                       @JsonProperty(value = "name") String name,
                                       @JsonProperty(value = "balance") Double balance,
                                       @JsonProperty("measurement_units") String measurementUnits,
                                       @JsonProperty("category_id") Long categoryId,
                                       @JsonProperty("price") Double price) {
        this.id = id;
        this.name = name;
        this.balance = balance;
        this.measurementUnits = measurementUnits;
        this.categoryId = categoryId;
        this.price = price;
    }
}