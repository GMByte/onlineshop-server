package com.keepcode.keepcodeshop.dto.views.category;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CategoryUpdateOrResponseView {

    private Long id;
    private String name;

    @JsonCreator
    public CategoryUpdateOrResponseView(@JsonProperty(value = "id",required = true) Long id,
                                        @JsonProperty(value = "name", required = true) String name) {
        this.id = id;
        this.name = name;
    }
}