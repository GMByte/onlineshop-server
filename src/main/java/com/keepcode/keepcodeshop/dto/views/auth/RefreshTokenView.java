package com.keepcode.keepcodeshop.dto.views.auth;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RefreshTokenView {

    private String refreshToken;

    @JsonCreator
    public RefreshTokenView(@JsonProperty(value = "refresh_token", required = true) String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
