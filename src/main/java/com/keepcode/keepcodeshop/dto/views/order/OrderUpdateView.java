package com.keepcode.keepcodeshop.dto.views.order;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter @Setter
public class OrderUpdateView {

    private Long id;
    private Long clientId;
    private String rowStatusCode;
    private String postNumber;

    @JsonCreator
    public OrderUpdateView(@JsonProperty(value = "id", required = true) Long id,
                           @JsonProperty("client_id") Long clientId,
                           @JsonProperty("row_status") String rowStatusCode,
                           @JsonProperty("post_number") String postNumber) {
        this.id = id;
        this.clientId = clientId;
        this.rowStatusCode = rowStatusCode;
        this.postNumber = postNumber;
    }
}
