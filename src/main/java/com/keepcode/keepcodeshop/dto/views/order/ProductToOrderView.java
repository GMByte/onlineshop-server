package com.keepcode.keepcodeshop.dto.views.order;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter @Setter
public class ProductToOrderView {

    private Long orderId;
    private Long productId;
    private Double amount;

    @JsonCreator
    public ProductToOrderView(@JsonProperty(value = "order_id", required = true) Long orderId,
                              @JsonProperty(value = "product_id", required = true) Long productId,
                              @JsonProperty(value = "amount", required = true) Double amount) {
        this.orderId = orderId;
        this.productId = productId;
        this.amount = amount;
    }
}
