package com.keepcode.keepcodeshop.dto.views.category;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CategoryCreateView {

    private String name;

    @JsonCreator
    public CategoryCreateView(@JsonProperty(value = "name", required = true) String name) {
        this.name = name;
    }
}