package com.keepcode.keepcodeshop.dto.views.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRoleAssignView {
    private Long userId;
    private String role;

    @JsonCreator
    public UserRoleAssignView(@JsonProperty(value = "user_id", required = true) Long userId,
                              @JsonProperty(value = "role", required = true) String role) {
        this.userId = userId;
        this.role = role;
    }
}