package com.keepcode.keepcodeshop.dto.views.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserResponseView {

    private Long id;

    private String username;

    private String role;

    @JsonProperty(value = "add_date")
    private Timestamp addDate;

    @JsonProperty(value = "last_login_date")
    private Timestamp lastLoginDate;
}
