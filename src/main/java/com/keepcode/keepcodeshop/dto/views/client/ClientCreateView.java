package com.keepcode.keepcodeshop.dto.views.client;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ClientCreateView {

    private String fullName;

    private String contactPhone;

    @JsonCreator
    public ClientCreateView(@JsonProperty(value = "full_name", required = true) String fullName,
                            @JsonProperty(value = "contact_phone", required = true) String contactPhone) {
        this.fullName = fullName;
        this.contactPhone = contactPhone;
    }
}