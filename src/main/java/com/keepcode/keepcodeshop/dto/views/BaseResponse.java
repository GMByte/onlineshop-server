package com.keepcode.keepcodeshop.dto.views;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class BaseResponse {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("body")
    private Object body;

    @JsonProperty("header")
    private ResponseHeader header;

    @Getter @Setter
    @NoArgsConstructor
    public static class ResponseHeader {

        private Integer statusCode;
        private String message;
        private String traceId;

        @JsonCreator
        public ResponseHeader(@JsonProperty(value = "status_code") Integer statusCode,
                              @JsonProperty(value = "message") String message,
                              @JsonProperty(value = "trace_id") String traceId) {
            this.statusCode = statusCode;
            this.message = message;
            this.traceId = traceId;
        }
    }
}