package com.keepcode.keepcodeshop.dto.views.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.keepcode.keepcodeshop.dto.views.product.ProductUpdateOrResponseView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class OrderResponseView {
    private Long id;

    @JsonProperty(value = "client_id")
    private Long clientId;

    @JsonProperty(value = "price_total")
    private Double priceTotal;

    @JsonProperty(value = "add_date")
    private Timestamp addDate;

    private String number;

    @JsonProperty(value = "row_status")
    private String rowStatusCode;

    @JsonProperty(value = "post_number")
    private String postNumber;

    @JsonProperty(value = "products_on_order")
    private List<ProductOnOrderView> productsOnOrder;

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter @Setter
    public static class ProductOnOrderView {
       private ProductUpdateOrResponseView product;
       private Double amount;
    }
}
