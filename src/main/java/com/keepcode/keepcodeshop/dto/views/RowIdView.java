package com.keepcode.keepcodeshop.dto.views;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter @Setter
@RequiredArgsConstructor
public class RowIdView {
    @JsonProperty("processed_row_id")
    private final Long processedRowId;
}
