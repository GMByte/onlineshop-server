package com.keepcode.keepcodeshop.dto.views.auth;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UserCreateView {

    private String email;
    private String password;

    @JsonCreator
    public UserCreateView(@JsonProperty(value = "email", required = true) String email,
                          @JsonProperty(value = "password", required = true) String password) {
        this.email = email;
        this.password = password;
    }
}
