package com.keepcode.keepcodeshop.dto.views.client;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Builder
@AllArgsConstructor
@Getter @Setter
public class ClientUpdateOrResponseView {

    private Long id;
    private String fullName;
    private String contactPhone;

    @JsonProperty(value = "user_id", access = JsonProperty.Access.READ_ONLY)
    private Long userId;

    @JsonProperty(value = "add_date", access = JsonProperty.Access.READ_ONLY)
    private Timestamp addDate;

    @JsonCreator
    public ClientUpdateOrResponseView(@JsonProperty(value = "id", required = true) Long id,
                                      @JsonProperty("full_name") String fullName,
                                      @JsonProperty("contact_phone") String contactPhone) {
        this.id = id;
        this.fullName = fullName;
        this.contactPhone = contactPhone;
    }
}
