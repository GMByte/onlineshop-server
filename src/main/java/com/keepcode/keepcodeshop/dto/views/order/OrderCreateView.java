package com.keepcode.keepcodeshop.dto.views.order;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Getter @Setter
public class OrderCreateView {

    private Long clientId;
    private List<Item> items;

    @JsonCreator
    public OrderCreateView(@JsonProperty(value = "client_id", required = true) Long clientId,
                           @JsonProperty(value = "items", required = true) List<Item> items) {
        this.clientId = clientId;
        this.items = items;
    }

    @Builder
    @Getter @Setter
    public static class Item {

        private Long productId;
        private Double amount;

        @JsonCreator
        public Item(@JsonProperty(value = "product_id", required = true) Long productId,
                    @JsonProperty(value = "amount", required = true) Double amount) {
            this.productId = productId;
            this.amount = amount;
        }
    }
}
