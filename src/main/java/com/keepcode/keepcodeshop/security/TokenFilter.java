package com.keepcode.keepcodeshop.security;

import com.keepcode.keepcodeshop.exceptions.TokenException;
import lombok.RequiredArgsConstructor;
import lombok.var;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.keepcode.keepcodeshop.tracing.TraceIdContextHolder.getTraceId;
import static com.keepcode.keepcodeshop.utils.ResponseUtils.convertObjectToJson;
import static com.keepcode.keepcodeshop.utils.ResponseUtils.respondError;

@Component
@RequiredArgsConstructor
public class TokenFilter extends GenericFilterBean {

    private final TokenResolver tokenResolver;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        try {
            var token = tokenResolver.getTokenFromRequest((HttpServletRequest) request);
            if (token.isPresent()) {
                var authentication = tokenResolver.resolveAuthenticationForTokenOrThrow(token.get());
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        } catch (TokenException tokenException) {
            SecurityContextHolder.clearContext();
            setErrorResponse(response, tokenException);
            throw tokenException;
        }
        if (isCorsRequest(request)) {
            setCorsResponse(response);
        } else {
            filterChain.doFilter(request, response);
        }
    }

    private void setErrorResponse(ServletResponse response, TokenException tokenException) throws IOException {
        var servletResponse = (HttpServletResponse) response;
        servletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        servletResponse.setCharacterEncoding("UTF-8");
        servletResponse.setContentType("application/json");
        servletResponse.getWriter().write(convertObjectToJson(respondError(tokenException, getTraceId())));
    }

    private boolean isCorsRequest(ServletRequest request) {
        return ((HttpServletRequest) request).getMethod().equals("OPTIONS");
    }

    private void setCorsResponse(ServletResponse response) {
        var servletResponse = (HttpServletResponse) response;
        servletResponse.setHeader("Access-Control-Allow-Origin", "*");
        servletResponse.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
        servletResponse.setHeader("Access-Control-Allow-Headers", "Authorization, Content-Type");
        servletResponse.setStatus(HttpServletResponse.SC_OK);
    }
}