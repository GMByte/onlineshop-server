package com.keepcode.keepcodeshop.security;

import com.keepcode.keepcodeshop.exceptions.ApiException;
import com.keepcode.keepcodeshop.exceptions.TokenException;
import com.keepcode.keepcodeshop.security.managers.StoredAccessTokenManger;
import lombok.RequiredArgsConstructor;
import lombok.var;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.ValidationException;
import java.util.Optional;

import static com.keepcode.keepcodeshop.exceptions.TokenException.ErrorMessage.TOKEN_IS_EXPIRED_OR_INVALID;
import static com.keepcode.keepcodeshop.utils.TokenUtils.getBearerTokenValue;
import static com.keepcode.keepcodeshop.utils.TokenUtils.isValidBearerTokenHeaderValue;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.Optional.ofNullable;

@Component
@RequiredArgsConstructor
public class TokenResolver {

    private static final String NULL_TOKEN_MESSAGE = "Не передан токен";

    @Value("${auth.header}")
    private String authorizationHeader;

    private final StoredAccessTokenManger storedAccessTokenManger;
    private final JwtTokenProvider jwtTokenProvider;

    public Authentication resolveAuthenticationForTokenOrThrow(String token) throws TokenException {
        requireNonNull(token, NULL_TOKEN_MESSAGE);
        try {
            var authentication = getAuthenticationForJwtTokenIfValid(token);
            if (authentication.isPresent()) {
                return authentication.get();
            }
        } catch (TokenException tokenException) {
            var authentication = getAuthenticationForStoredTokenIfExists(token);
            if (authentication.isPresent()) {
                return authentication.get();
            }
            throw tokenException;
        }
        throw new TokenException(TOKEN_IS_EXPIRED_OR_INVALID);
    }

    public Optional<String> getTokenFromRequest(HttpServletRequest request) {
        var tokenHeader = request.getHeader(authorizationHeader);
        if (!isValidBearerTokenHeaderValue(tokenHeader)) {
            return empty();
        }
        return of(getBearerTokenValue(tokenHeader));
    }

    private Optional<Authentication> getAuthenticationForStoredTokenIfExists(String token) throws TokenException {
        try {
            var authentication = storedAccessTokenManger.validateAndGetAuthentication(token);
            return ofNullable(authentication);
        } catch (TokenException exception) {
            return empty();
        }
    }

    private Optional<Authentication> getAuthenticationForJwtTokenIfValid(String token) throws TokenException {
        if (jwtTokenProvider.validateToken(token)) {
            var authentication  = jwtTokenProvider.getAuthentication(token);
            if (nonNull(authentication)) {
                return of(authentication);
            }
        }
        return empty();
    }
}