package com.keepcode.keepcodeshop.security;

import com.keepcode.keepcodeshop.configuration.SecurityTokenConfig;
import com.keepcode.keepcodeshop.exceptions.ApiException;
import com.keepcode.keepcodeshop.exceptions.TokenException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.var;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Base64;
import java.util.Date;

import static com.keepcode.keepcodeshop.exceptions.TokenException.ErrorMessage.TOKEN_IS_EXPIRED_OR_INVALID;
import static com.keepcode.keepcodeshop.utils.TimeUtils.minutesToMills;

@Component
public class JwtTokenProvider {

    private final UserDetailsService userDetailsService;
    private final SecurityTokenConfig securityTokenConfig;

    private String secretKey;

    @Value("${jwt.access.expiration.minutes}")
    private long desiredJwtExpTimeInMinutes;

    public JwtTokenProvider(@Qualifier("userDetailsServiceImpl") UserDetailsService userDetailsService,
                            SecurityTokenConfig securityTokenConfig) {
        this.userDetailsService = userDetailsService;
        this.securityTokenConfig = securityTokenConfig;
    }

    @PostConstruct
    protected void init() {
        secretKey = Base64.getEncoder().encodeToString(securityTokenConfig.getSecretKey().getBytes());
    }

    public String createToken(String username, String role) {
        var claims = Jwts.claims().setSubject(username);
        claims.put("role", role);
        var issuedAt = new Date();
        var expiration = new Date(issuedAt.getTime() + minutesToMills(desiredJwtExpTimeInMinutes));
        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(issuedAt)
                .setExpiration(expiration)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    public boolean validateToken(String token) throws TokenException {
        try {
            var claimsJws = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            return !claimsJws.getBody().getExpiration().before(new Date());
        } catch (JwtException | IllegalArgumentException e) {
            throw new TokenException(TOKEN_IS_EXPIRED_OR_INVALID);
        }
    }

    public Authentication getAuthentication(String token) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(getUserName(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    public String getUserName(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    }
}
