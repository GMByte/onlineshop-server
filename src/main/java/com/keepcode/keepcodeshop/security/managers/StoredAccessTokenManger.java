package com.keepcode.keepcodeshop.security.managers;

import com.keepcode.keepcodeshop.exceptions.TokenException;
import org.springframework.security.core.Authentication;

public interface StoredAccessTokenManger extends TokenManager {
    Authentication validateAndGetAuthentication(String token) throws TokenException;
}