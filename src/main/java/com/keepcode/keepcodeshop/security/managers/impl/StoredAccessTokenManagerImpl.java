package com.keepcode.keepcodeshop.security.managers.impl;

import com.keepcode.keepcodeshop.configuration.SecurityTokenConfig;
import com.keepcode.keepcodeshop.security.managers.StoredAccessTokenManger;
import lombok.var;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.token.Token;
import org.springframework.security.core.token.TokenService;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import static com.keepcode.keepcodeshop.utils.TimeUtils.minutesToMills;

@Component
@Scope("singleton")
public class StoredAccessTokenManagerImpl extends MapStoredUserTokenManager implements StoredAccessTokenManger {

    private final UserDetailsService userDetailsService;
    private final SecurityTokenConfig securityTokenConfig;
    private final Map<String, Token> inMemoryBearerTokens = new ConcurrentHashMap<>();

    public StoredAccessTokenManagerImpl(TokenService tokenService,
                                        UserDetailsService userDetailsService,
                                        SecurityTokenConfig securityTokenConfig) {
        super(tokenService);
        this.userDetailsService = userDetailsService;
        this.securityTokenConfig = securityTokenConfig;
    }

    @Override
    public Authentication validateAndGetAuthentication(String token) {
        var username = validateAndGetUsername(token);
        var userDetails = userDetailsService.loadUserByUsername(username);
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    @Scheduled(fixedDelay = 10, timeUnit = TimeUnit.MINUTES)
    private void deleteExpired() {
        deleteExpiredTokensFrom(inMemoryBearerTokens);
    }

    @Override
    protected Map<String, Token> getTokenMap() {
        return inMemoryBearerTokens;
    }

    @Override
    protected Long getExpTimeMills() {
        return minutesToMills(securityTokenConfig.getDesiredBearerExpTimeInMinutes());
    }
}