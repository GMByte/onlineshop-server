package com.keepcode.keepcodeshop.security.managers.impl;

import com.keepcode.keepcodeshop.configuration.SecurityTokenConfig;
import com.keepcode.keepcodeshop.security.managers.RefreshTokenManager;
import lombok.var;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.token.Token;
import org.springframework.security.core.token.TokenService;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import static com.keepcode.keepcodeshop.utils.TimeUtils.minutesToMills;

@Component
@Scope("singleton")
public class RefreshTokenManagerImpl extends MapStoredUserTokenManager implements RefreshTokenManager {

    private final SecurityTokenConfig tokenConfig;
    private final Map<String, Token> refreshTokens = new ConcurrentHashMap<>();

    public RefreshTokenManagerImpl(TokenService tokenService, SecurityTokenConfig tokenConfig) {
        super(tokenService);
        this.tokenConfig = tokenConfig;
    }

    @Override
    public Token updateAndGetNewIfValid(String token) {
        var username = validateAndGetUsername(token);
        removeTokenByUsername(username);
        return generateAndStoreToken(username);
    }

    @Scheduled(fixedDelay = 10, timeUnit = TimeUnit.MINUTES)
    private void deleteExpired() {
        deleteExpiredTokensFrom(refreshTokens);
    }

    @Override
    protected Map<String, Token> getTokenMap() {
        return refreshTokens;
    }

    @Override
    protected Long getExpTimeMills() {
        return minutesToMills(tokenConfig.getDesiredRefreshExpTimeInMinutes());
    }
}