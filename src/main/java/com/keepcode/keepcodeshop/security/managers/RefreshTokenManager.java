package com.keepcode.keepcodeshop.security.managers;

import org.springframework.security.core.token.Token;

import javax.xml.bind.ValidationException;

public interface RefreshTokenManager extends TokenManager {
    Token updateAndGetNewIfValid(String token) throws ValidationException;
}