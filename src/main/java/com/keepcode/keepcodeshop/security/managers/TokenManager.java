package com.keepcode.keepcodeshop.security.managers;

import com.keepcode.keepcodeshop.exceptions.TokenException;
import org.springframework.security.core.token.Token;

public interface TokenManager {
    Token generateAndStoreToken(String username);

    void removeToken(String key);

    void removeTokenByUsername(String username);

    String validateAndGetUsername(String key) throws TokenException;
}