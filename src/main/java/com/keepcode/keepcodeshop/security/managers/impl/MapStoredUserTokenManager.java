package com.keepcode.keepcodeshop.security.managers.impl;

import com.keepcode.keepcodeshop.exceptions.TokenException;
import com.keepcode.keepcodeshop.security.managers.TokenManager;
import lombok.RequiredArgsConstructor;
import lombok.var;
import org.springframework.security.core.token.Token;
import org.springframework.security.core.token.TokenService;

import java.util.Date;
import java.util.Map;

import static com.keepcode.keepcodeshop.exceptions.TokenException.ErrorMessage.INVALID_TOKEN;

@RequiredArgsConstructor
public abstract class MapStoredUserTokenManager implements TokenManager {

    private final TokenService tokenService;

    protected abstract Map<String, Token> getTokenMap();
    protected abstract Long getExpTimeMills();

    @Override
    public Token generateAndStoreToken(String username) {
        var tokenMap = getTokenMap();
        var generatedToken = tokenService.allocateToken(username);
        tokenMap.put(generatedToken.getKey(), generatedToken);
        return generatedToken;
    }

    @Override
    public void removeToken(String key) {
        getTokenMap().remove(key);
    }

    @Override
    public void removeTokenByUsername(String username) {
        getTokenMap().entrySet()
                .removeIf(entry -> entry.getValue().getExtendedInformation().equals(username));
    }

    @Override
    public String validateAndGetUsername(String key) throws TokenException {
       Token token = validateKeyAndGetToken(key);
       if (isExpired(token)) {
           removeToken(key);
           throw new TokenException(INVALID_TOKEN);
       }
       return token.getExtendedInformation();
    }

    protected Token validateKeyAndGetToken(String key) throws TokenException {
        var tokenMap = getTokenMap();
        if (!tokenMap.containsKey(key)) {
            throw new TokenException(INVALID_TOKEN);
        }
       Token decodedToken = tokenService.verifyToken(key);
       if (!getTokenMap().containsValue(decodedToken)) {
           throw new TokenException(INVALID_TOKEN);
       }
       return decodedToken;
    }

    private boolean isExpired(Token token) {
       var expTime = token.getKeyCreationTime() + getExpTimeMills();
       return new Date().getTime() > expTime;
    }

    protected void deleteExpiredTokensFrom(Map<String, Token> tokenMap) {
        tokenMap.entrySet().removeIf(entry -> isExpired(entry.getValue()));
    }
}