package com.keepcode.keepcodeshop.tracing;

/**
 * Класс для хранения идентификатора трассировки в пределах одного вызова.
 */
public class TraceIdContextHolder {

    private TraceIdContextHolder() {
    }

    private static final InheritableThreadLocal<String> traceId = new InheritableThreadLocal<>();

    public static void setTraceId(String id) {
        traceId.set(id);
    }

    public static String getTraceId() {
        return traceId.get();
    }
}