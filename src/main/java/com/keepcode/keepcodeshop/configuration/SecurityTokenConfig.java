package com.keepcode.keepcodeshop.configuration;

import lombok.Getter;
import lombok.var;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.token.KeyBasedPersistenceTokenService;
import org.springframework.security.core.token.TokenService;

import java.security.SecureRandom;

@Configuration
@Getter
public class SecurityTokenConfig {

    @Value("${server.secret}")
    private String secretKey;

    @Value("${server.integer}")
    private Integer serverInt;

    @Value("${refresh.expiration.minutes}")
    private Long desiredRefreshExpTimeInMinutes;

    @Value("${bearer.token.expiration.minutes}")
    private Long desiredBearerExpTimeInMinutes;

    @Bean
    public TokenService tokenService() {
        var tokenService = new KeyBasedPersistenceTokenService();
        tokenService.setServerSecret(secretKey);
        tokenService.setServerInteger(serverInt);
        tokenService.setSecureRandom(new SecureRandom());
        return tokenService;
    }
}
