package com.keepcode.keepcodeshop.configuration;

import com.keepcode.keepcodeshop.security.TokenFilterConfigurer;
import lombok.Getter;
import lombok.var;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static com.keepcode.keepcodeshop.constants.UrlConstants.BASE_CATEGORIES_URL_MATCHER;
import static com.keepcode.keepcodeshop.constants.UrlConstants.BASE_CLIENTS_URL_MATCHER;
import static com.keepcode.keepcodeshop.constants.UrlConstants.BASE_ORDERS_URL_MATCHER;
import static com.keepcode.keepcodeshop.constants.UrlConstants.BASE_PRODUCTS_URL_MARCHER;
import static com.keepcode.keepcodeshop.constants.UrlConstants.BASE_USERS_URL_MATCHER;
import static com.keepcode.keepcodeshop.constants.UrlConstants.CREATE_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.DELETE_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.GET_ALL_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.UPDATE_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.enums.Permission.CLIENTS_READ;
import static com.keepcode.keepcodeshop.constants.enums.Permission.PRODUCTS_MANAGE;
import static com.keepcode.keepcodeshop.constants.enums.Permission.USERS_READ;
import static com.keepcode.keepcodeshop.constants.enums.Permission.USERS_WRITE;
import static com.keepcode.keepcodeshop.controllers.ClientController.LIST_ALL_EXISTING_CLIENTS;
import static com.keepcode.keepcodeshop.controllers.OrderController.ADD_PRODUCT_TO_ORDER;
import static com.keepcode.keepcodeshop.controllers.OrderController.CANCEL_ORDER;
import static com.keepcode.keepcodeshop.controllers.OrderController.DELETE_PRODUCT_FROM_ORDER_PATH;
import static com.keepcode.keepcodeshop.controllers.OrderController.GET_ORDERS_BY_CLIENT_ID;
import static com.keepcode.keepcodeshop.controllers.OrderController.GET_ORDERS_BY_USER_ID;
import static com.keepcode.keepcodeshop.controllers.UserController.ASSIGN_ROLE;
import static com.keepcode.keepcodeshop.controllers.UserController.CHANGE_PASSWORD;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Getter
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final TokenFilterConfigurer jwtConfigurer;
    private final SecurityTokenConfig securityTokenConfig;

    public WebSecurityConfig(TokenFilterConfigurer jwtConfigurer,
                             SecurityTokenConfig webSecurityTokenConfiguration) {
        this.jwtConfigurer = jwtConfigurer;
        this.securityTokenConfig = webSecurityTokenConfiguration;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        var manageProductsAuthority = PRODUCTS_MANAGE.getAuthority();
        var clientsRead = CLIENTS_READ.getAuthority();
        var usersRead = USERS_READ.getAuthority();
        var usersWrite = USERS_WRITE.getAuthority();
        http
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                //Categories
                .mvcMatchers(HttpMethod.POST, BASE_CATEGORIES_URL_MATCHER).hasAuthority(manageProductsAuthority)
                .mvcMatchers(HttpMethod.DELETE, BASE_CATEGORIES_URL_MATCHER).hasAuthority(manageProductsAuthority)
                .mvcMatchers(HttpMethod.GET, BASE_CATEGORIES_URL_MATCHER).authenticated()
                //Clients
                .mvcMatchers(BASE_CLIENTS_URL_MATCHER + LIST_ALL_EXISTING_CLIENTS).hasAuthority(clientsRead)
                .mvcMatchers(BASE_CLIENTS_URL_MATCHER + GET_ALL_REQUEST_PATH).authenticated()
                .mvcMatchers(HttpMethod.POST, BASE_CLIENTS_URL_MATCHER).authenticated()
                .mvcMatchers(HttpMethod.DELETE, BASE_CLIENTS_URL_MATCHER).authenticated()
                //Orders
                .mvcMatchers(BASE_ORDERS_URL_MATCHER + UPDATE_REQUEST_PATH,
                        BASE_ORDERS_URL_MATCHER + DELETE_REQUEST_PATH).hasAuthority(manageProductsAuthority)
                .mvcMatchers(BASE_ORDERS_URL_MATCHER + GET_ORDERS_BY_USER_ID,
                        BASE_ORDERS_URL_MATCHER + GET_ORDERS_BY_CLIENT_ID).hasAuthority(clientsRead)
                .mvcMatchers(getAllRoleOrderPaths()).authenticated()
                //Products
                .mvcMatchers(HttpMethod.POST, BASE_PRODUCTS_URL_MARCHER).hasAuthority(manageProductsAuthority)
                .mvcMatchers(HttpMethod.DELETE, BASE_PRODUCTS_URL_MARCHER).hasAuthority(manageProductsAuthority)
                .mvcMatchers(HttpMethod.GET, BASE_PRODUCTS_URL_MARCHER).authenticated()
                //Users
                .mvcMatchers(BASE_USERS_URL_MATCHER + GET_ALL_REQUEST_PATH).hasAuthority(usersRead)
                .mvcMatchers(BASE_USERS_URL_MATCHER + ASSIGN_ROLE).hasAuthority(usersWrite)
                .mvcMatchers(BASE_USERS_URL_MATCHER + CHANGE_PASSWORD).authenticated()
                //Auth
                .mvcMatchers("**/auth/**").permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .apply(jwtConfigurer);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() { return new BCryptPasswordEncoder(); }

    private String[] getAllRoleOrderPaths() {
        return new String[]{BASE_ORDERS_URL_MATCHER + CREATE_REQUEST_PATH,
                BASE_ORDERS_URL_MATCHER + ADD_PRODUCT_TO_ORDER,
                BASE_ORDERS_URL_MATCHER + GET_ALL_REQUEST_PATH,
                BASE_ORDERS_URL_MATCHER + CANCEL_ORDER,
                BASE_ORDERS_URL_MATCHER + DELETE_PRODUCT_FROM_ORDER_PATH};
    }
}