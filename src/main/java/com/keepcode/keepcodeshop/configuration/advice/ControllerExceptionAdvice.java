package com.keepcode.keepcodeshop.configuration.advice;

import com.keepcode.keepcodeshop.dto.views.BaseResponse;
import com.keepcode.keepcodeshop.exceptions.ApiException;
import com.keepcode.keepcodeshop.exceptions.BaseException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import static com.keepcode.keepcodeshop.exceptions.ApiException.ErrorMessage.UNKNOWN_ERROR;
import static com.keepcode.keepcodeshop.tracing.TraceIdContextHolder.getTraceId;
import static com.keepcode.keepcodeshop.utils.ResponseUtils.respondError;

@Slf4j
@ControllerAdvice
public class ControllerExceptionAdvice {
    @ExceptionHandler(Exception.class)
    public @ResponseBody BaseResponse handleException(Exception ex) {
        log.error(ex.getMessage(), ex);
        if (ex instanceof BaseException) {
            return respondError((BaseException) ex, getTraceId());
        }
        if (ex instanceof HttpMessageNotReadableException) {
            return respondError(new ApiException(ex.getMessage()), getTraceId());
        }
        return respondError(new ApiException(UNKNOWN_ERROR), getTraceId());
    }
}