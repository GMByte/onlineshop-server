package com.keepcode.keepcodeshop.configuration;

import com.keepcode.keepcodeshop.filters.FlowInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
@EnableScheduling
public class AppContextConfiguration implements WebMvcConfigurer {
    @Bean
    public FlowInterceptor requestFlowInterceptor() {
        return new FlowInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(requestFlowInterceptor());
    }
}