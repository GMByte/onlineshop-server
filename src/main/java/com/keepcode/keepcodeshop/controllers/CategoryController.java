package com.keepcode.keepcodeshop.controllers;

import com.keepcode.keepcodeshop.dto.views.BaseResponse;
import com.keepcode.keepcodeshop.dto.views.category.CategoryCreateView;
import com.keepcode.keepcodeshop.dto.views.category.CategoryUpdateOrResponseView;
import com.keepcode.keepcodeshop.services.CategoryService;
import lombok.RequiredArgsConstructor;
import lombok.var;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import static com.keepcode.keepcodeshop.constants.UrlConstants.CATEGORY_BASE_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.CREATE_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.DELETE_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.GET_ALL_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.GET_BY_ID_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.READ_OPERATION_PREFIX;
import static com.keepcode.keepcodeshop.constants.UrlConstants.UPDATE_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.WRITE_OPERATION_PREFIX;
import static com.keepcode.keepcodeshop.tracing.TraceIdContextHolder.getTraceId;
import static com.keepcode.keepcodeshop.utils.ResponseUtils.respondOk;
import static com.keepcode.keepcodeshop.utils.ResponseUtils.respondOkWithBody;
import static com.keepcode.keepcodeshop.utils.ResponseUtils.respondOkWithId;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class CategoryController {

    private static final String READ_CATEGORY_BASE_PATH = CATEGORY_BASE_PATH + READ_OPERATION_PREFIX;
    private static final String WRITE_CATEGORY_BASE_PATH = CATEGORY_BASE_PATH + WRITE_OPERATION_PREFIX;

    private final CategoryService categoryService;

    @GetMapping(path = READ_CATEGORY_BASE_PATH + GET_ALL_REQUEST_PATH, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse listCategories() {
       var categories = categoryService.listCategories();
       return respondOkWithBody(categories, getTraceId());
    }

    @GetMapping(path = READ_CATEGORY_BASE_PATH + GET_BY_ID_REQUEST_PATH, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse getById(@PathVariable(value = "id") Long id) {
        var result = categoryService.findById(id);
        return respondOkWithBody(result, getTraceId());
    }

    @PostMapping(path = WRITE_CATEGORY_BASE_PATH + CREATE_REQUEST_PATH, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse create(@RequestBody CategoryCreateView view) {
        var result = categoryService.create(view);
        return respondOkWithId(result.getId(), getTraceId());
    }

    @PostMapping(path = WRITE_CATEGORY_BASE_PATH + UPDATE_REQUEST_PATH, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse update(@RequestBody CategoryUpdateOrResponseView view) {
        var result = categoryService.update(view);
        return respondOkWithId(result.getId(), getTraceId());
    }

    @DeleteMapping(path = WRITE_CATEGORY_BASE_PATH + DELETE_REQUEST_PATH, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse delete(@RequestParam("id") Long id, @RequestParam(value = "move_to_id", required = false) Long moveToId) {
        categoryService.delete(id, moveToId);
        return respondOk(getTraceId());
    }
}