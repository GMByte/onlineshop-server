package com.keepcode.keepcodeshop.controllers;

import com.keepcode.keepcodeshop.dto.entities.User;
import com.keepcode.keepcodeshop.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.var;
import org.springframework.security.core.context.SecurityContextHolder;

@RequiredArgsConstructor
public class AuthenticatedUserController {

    protected final UserService userService;

    protected User getLoggedInUser() {
        var auth = SecurityContextHolder.getContext().getAuthentication();
        return userService.getUserByUsername(auth.getName());
    }
}
