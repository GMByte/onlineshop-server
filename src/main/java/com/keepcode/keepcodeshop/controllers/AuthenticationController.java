package com.keepcode.keepcodeshop.controllers;

import com.keepcode.keepcodeshop.dto.entities.User;
import com.keepcode.keepcodeshop.dto.views.BaseResponse;
import com.keepcode.keepcodeshop.dto.views.auth.AuthenticationRequestView;
import com.keepcode.keepcodeshop.dto.views.auth.RefreshTokenView;
import com.keepcode.keepcodeshop.dto.views.auth.UserCreateView;
import com.keepcode.keepcodeshop.exceptions.ApiException;
import com.keepcode.keepcodeshop.exceptions.RequestValidationException;
import com.keepcode.keepcodeshop.exceptions.TokenException;
import com.keepcode.keepcodeshop.repositories.UserRepository;
import com.keepcode.keepcodeshop.security.JwtTokenProvider;
import com.keepcode.keepcodeshop.security.TokenResolver;
import com.keepcode.keepcodeshop.security.managers.RefreshTokenManager;
import com.keepcode.keepcodeshop.security.managers.StoredAccessTokenManger;
import com.keepcode.keepcodeshop.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.var;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.ValidationException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.keepcode.keepcodeshop.exceptions.ApiException.ErrorMessage.INVALID_LOGIN;
import static com.keepcode.keepcodeshop.exceptions.RequestValidationException.ErrorMessage.INVALID_USERNAME_OR_PASSWORD;
import static com.keepcode.keepcodeshop.exceptions.TokenException.ErrorMessage.TOKEN_NOT_PRESENT;
import static com.keepcode.keepcodeshop.tracing.TraceIdContextHolder.getTraceId;
import static com.keepcode.keepcodeshop.utils.ResponseUtils.respondOk;
import static com.keepcode.keepcodeshop.utils.ResponseUtils.respondOkWithBody;
import static com.keepcode.keepcodeshop.utils.TimeUtils.currentTimestamp;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/auth")
public class AuthenticationController {

    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;
    private final JwtTokenProvider jwtTokenProvider;
    private final RefreshTokenManager refreshTokenManager;
    private final StoredAccessTokenManger storedAccessTokenManager;
    private final TokenResolver tokenResolver;
    private final UserService userService;

    @PostMapping("/sign_up")
    public @ResponseBody BaseResponse signUp(@RequestBody UserCreateView view) throws RequestValidationException {
        userService.create(view);
        return respondOk(getTraceId());
    }

    @PostMapping("/login")
    public @ResponseBody BaseResponse login(@RequestBody AuthenticationRequestView view) throws RequestValidationException {
        try {
            var user = authenticateAndGetUser(view);
            var refreshToken = refreshTokenManager.generateAndStoreToken(user.getName());
            return generateStoredTokenAndGetResponse(user, refreshToken.getKey());
        } catch (AuthenticationException e) {
            throw new RequestValidationException(INVALID_USERNAME_OR_PASSWORD);
        }
    }

    @PostMapping("/refresh")
    public @ResponseBody BaseResponse refresh(@RequestBody RefreshTokenView view) throws ValidationException {
        var newRefreshToken = refreshTokenManager.updateAndGetNewIfValid(view.getRefreshToken());
        var user = userRepository.getUserByName(newRefreshToken.getExtendedInformation());
        if (user == null) {
            throw new ApiException(INVALID_LOGIN);
        }
        return generateStoredTokenAndGetResponse(user, newRefreshToken.getKey());
    }

    @PostMapping("/login_jwt")
    public @ResponseBody BaseResponse loginJwt(@RequestBody AuthenticationRequestView view) throws RequestValidationException {
        try {
            var user = authenticateAndGetUser(view);
            var refreshToken = refreshTokenManager.generateAndStoreToken(view.getEmail());
            return generateJwtTokenAndGetResponse(user, refreshToken.getKey());
        } catch (AuthenticationException e) {
            throw new RequestValidationException(INVALID_USERNAME_OR_PASSWORD);
        }
    }

    @PostMapping("/refresh_jwt")
    public @ResponseBody BaseResponse refreshJwt(@RequestBody RefreshTokenView view) throws ValidationException {
        var newRefreshToken = refreshTokenManager.updateAndGetNewIfValid(view.getRefreshToken());
        var user = userRepository.getUserByName(newRefreshToken.getExtendedInformation());
        if (user == null) {
            throw new ApiException(INVALID_LOGIN);
        }
        return generateJwtTokenAndGetResponse(user, newRefreshToken.getKey());
    }

    @PostMapping("/logout")
    public @ResponseBody BaseResponse logout(HttpServletRequest request, HttpServletResponse response) throws TokenException {
        var token = tokenResolver.getTokenFromRequest(request);
        if (!token.isPresent()) {
            throw new TokenException(TOKEN_NOT_PRESENT);
        }
        var authentication = tokenResolver.resolveAuthenticationForTokenOrThrow(token.get());
        var securityContextLogoutHandler = new SecurityContextLogoutHandler();
        securityContextLogoutHandler.logout(request, response, authentication);
        clearTokenStoragesByUsername(authentication.getName());
        return respondOk(getTraceId());
    }

    private User authenticateAndGetUser(AuthenticationRequestView view) throws AuthenticationException {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(view.getEmail(), view.getPassword()));
        var user = userRepository.getUserByName(view.getEmail());
        user.setLastLoginDate(currentTimestamp());
        userRepository.save(user);
        return user;
    }

    private void clearTokenStoragesByUsername(String username) {
        storedAccessTokenManager.removeTokenByUsername(username);
        refreshTokenManager.removeTokenByUsername(username);
    }

    private BaseResponse generateStoredTokenAndGetResponse(User user, String refreshToken) {
        var accessToken = storedAccessTokenManager.generateAndStoreToken(user.getName());
        return getGeneratedTokenResponse(user, accessToken.getKey(), refreshToken);
    }

    private BaseResponse generateJwtTokenAndGetResponse(User user, String refreshToken) {
        var accessToken = jwtTokenProvider.createToken(user.getName(), user.getRole().name());
        return getGeneratedTokenResponse(user, accessToken, refreshToken);
    }

    private BaseResponse getGeneratedTokenResponse(User user, String accessToken, String refreshToken) {
        Map<Object, Object> response = new HashMap<>();
        response.put("email", user.getName());
        response.put("access_token", accessToken);
        response.put("refresh_token", refreshToken);
        return respondOkWithBody(response, getTraceId());
    }
}