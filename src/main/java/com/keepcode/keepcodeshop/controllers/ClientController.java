package com.keepcode.keepcodeshop.controllers;

import com.keepcode.keepcodeshop.dto.views.BaseResponse;
import com.keepcode.keepcodeshop.dto.views.client.ClientCreateView;
import com.keepcode.keepcodeshop.dto.views.client.ClientUpdateOrResponseView;
import com.keepcode.keepcodeshop.exceptions.RequestValidationException;
import com.keepcode.keepcodeshop.services.ClientService;
import com.keepcode.keepcodeshop.services.UserService;
import lombok.var;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import static com.keepcode.keepcodeshop.constants.UrlConstants.CLIENT_BASE_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.CREATE_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.DELETE_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.GET_ALL_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.READ_OPERATION_PREFIX;
import static com.keepcode.keepcodeshop.constants.UrlConstants.UPDATE_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.WRITE_OPERATION_PREFIX;
import static com.keepcode.keepcodeshop.tracing.TraceIdContextHolder.getTraceId;
import static com.keepcode.keepcodeshop.utils.ResponseUtils.respondOk;
import static com.keepcode.keepcodeshop.utils.ResponseUtils.respondOkWithBody;
import static com.keepcode.keepcodeshop.utils.ResponseUtils.respondOkWithId;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/v1")
public class ClientController extends AuthenticatedUserController {

    private static final String READ_CLIENT_BASE_PATH = CLIENT_BASE_PATH + READ_OPERATION_PREFIX;
    private static final String WRITE_CLIENT_BASE_PATH = CLIENT_BASE_PATH + WRITE_OPERATION_PREFIX;
    public static final String LIST_ALL_EXISTING_CLIENTS = "/admin/all";

    private final ClientService clientService;

    public ClientController(UserService userService, ClientService clientService) {
        super(userService);
        this.clientService = clientService;
    }

    @GetMapping(path = READ_CLIENT_BASE_PATH + GET_ALL_REQUEST_PATH, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse findAllUserClients() {
        var result = clientService.listUserClients(getLoggedInUser());
        return respondOkWithBody(result, getTraceId());
    }

    @GetMapping(path = READ_CLIENT_BASE_PATH + LIST_ALL_EXISTING_CLIENTS)
    public @ResponseBody BaseResponse findAll() {
        var result = clientService.listClients();
        return respondOkWithBody(result, getTraceId());
    }

    @PostMapping(path = WRITE_CLIENT_BASE_PATH + CREATE_REQUEST_PATH, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse create(@RequestBody ClientCreateView view) throws RequestValidationException {
        var result = clientService.create(view, getLoggedInUser());
        return respondOkWithId(result.getId(), getTraceId());
    }

    @PostMapping(path = WRITE_CLIENT_BASE_PATH + UPDATE_REQUEST_PATH, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse update(@RequestBody ClientUpdateOrResponseView view) throws RequestValidationException {
        clientService.update(view, getLoggedInUser());
        return respondOk(getTraceId());
    }

    @DeleteMapping(path = WRITE_CLIENT_BASE_PATH + DELETE_REQUEST_PATH, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse delete(@RequestParam("id") Long clientId) {
        clientService.delete(clientId, getLoggedInUser());
        return respondOk(getTraceId());
    }
}