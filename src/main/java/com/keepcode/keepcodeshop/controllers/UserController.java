package com.keepcode.keepcodeshop.controllers;

import com.keepcode.keepcodeshop.dto.views.BaseResponse;
import com.keepcode.keepcodeshop.dto.views.user.UserPasswordChangeView;
import com.keepcode.keepcodeshop.dto.views.user.UserRoleAssignView;
import com.keepcode.keepcodeshop.exceptions.RequestValidationException;
import com.keepcode.keepcodeshop.services.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import static com.keepcode.keepcodeshop.constants.UrlConstants.GET_ALL_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.READ_OPERATION_PREFIX;
import static com.keepcode.keepcodeshop.constants.UrlConstants.USER_BASE_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.WRITE_OPERATION_PREFIX;
import static com.keepcode.keepcodeshop.tracing.TraceIdContextHolder.getTraceId;
import static com.keepcode.keepcodeshop.utils.ResponseUtils.respondOk;
import static com.keepcode.keepcodeshop.utils.ResponseUtils.respondOkWithBody;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/v1")
public class UserController extends AuthenticatedUserController {

    private static final String READ_USER_BASE_PATH = USER_BASE_PATH + READ_OPERATION_PREFIX;
    private static final String WRITE_USER_BASE_PATH = USER_BASE_PATH + WRITE_OPERATION_PREFIX;
    public static final String CHANGE_PASSWORD = "/change_password";
    public static final String ASSIGN_ROLE = "/assign_role";

    public UserController(UserService userService) {
        super(userService);
    }

    @GetMapping(path = READ_USER_BASE_PATH + GET_ALL_REQUEST_PATH, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse findAll() {
        return respondOkWithBody(userService.list(),getTraceId());
    }

    @PostMapping(path = WRITE_USER_BASE_PATH + CHANGE_PASSWORD, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse changePassword(@RequestBody UserPasswordChangeView view) throws RequestValidationException {
        userService.changePassword(view, getLoggedInUser());
        return respondOk(getTraceId());
    }

    @PostMapping(path = WRITE_USER_BASE_PATH + ASSIGN_ROLE, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse assignRole(@RequestBody UserRoleAssignView view) throws RequestValidationException {
        userService.assignUserRole(view);
        return respondOk(getTraceId());
    }
}