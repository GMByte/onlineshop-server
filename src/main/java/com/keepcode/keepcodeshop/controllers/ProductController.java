package com.keepcode.keepcodeshop.controllers;

import com.keepcode.keepcodeshop.dto.views.BaseResponse;
import com.keepcode.keepcodeshop.dto.views.product.ProductCreateView;
import com.keepcode.keepcodeshop.dto.views.product.ProductUpdateOrResponseView;
import com.keepcode.keepcodeshop.exceptions.RequestValidationException;
import com.keepcode.keepcodeshop.services.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.var;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import static com.keepcode.keepcodeshop.constants.AppConfigConstants.DEFAULT_PAGE_SIZE;
import static com.keepcode.keepcodeshop.constants.UrlConstants.CREATE_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.DELETE_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.GET_BY_ID_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.PRODUCT_BASE_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.READ_OPERATION_PREFIX;
import static com.keepcode.keepcodeshop.constants.UrlConstants.UPDATE_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.WRITE_OPERATION_PREFIX;
import static com.keepcode.keepcodeshop.tracing.TraceIdContextHolder.getTraceId;
import static com.keepcode.keepcodeshop.utils.ResponseUtils.respondOkWithBody;
import static com.keepcode.keepcodeshop.utils.ResponseUtils.respondOkWithId;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Controller
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class ProductController {

    private static final String READ_PRODUCT_BASE_PATH = PRODUCT_BASE_PATH + READ_OPERATION_PREFIX;
    private static final String WRITE_PRODUCT_BASE_PATH = PRODUCT_BASE_PATH + WRITE_OPERATION_PREFIX;

    private static final String GET_PRODUCT_PAGE_PATH = "/pages/{num}";

    private final ProductService productService;

    @PostMapping(path = WRITE_PRODUCT_BASE_PATH + CREATE_REQUEST_PATH, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse create(@RequestBody ProductCreateView view) throws RequestValidationException {
        var result = productService.create(view);
        return respondOkWithId(result.getId(), getTraceId());
    }

    @PostMapping(path = WRITE_PRODUCT_BASE_PATH + UPDATE_REQUEST_PATH, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse update(@RequestBody ProductUpdateOrResponseView view) throws RequestValidationException {
        var result = productService.update(view);
        return respondOkWithId(result.getId(), getTraceId());
    }

    @DeleteMapping(path = WRITE_PRODUCT_BASE_PATH + DELETE_REQUEST_PATH, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse delete(@RequestParam(value = "id") Long id) {
        var result = productService.delete(id);
        return respondOkWithId(result.getId(), getTraceId());
    }

    @GetMapping(path = READ_PRODUCT_BASE_PATH + GET_BY_ID_REQUEST_PATH, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse getById(@PathVariable("id") Long id) {
        var result = productService.findById(id);
        return respondOkWithBody(result, getTraceId());
    }

    @GetMapping(path = READ_PRODUCT_BASE_PATH + GET_PRODUCT_PAGE_PATH, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse getPage(@PathVariable("num") Integer pageNum,
                                              @RequestParam(value = "page_size", required = false) Integer pageSize,
                                              @RequestParam(value = "category_id", required = false) Long categoryId) {
        var pageSizeNum = pageSize != null ? pageSize : DEFAULT_PAGE_SIZE;
        var result = productService.getPage(pageNum, pageSizeNum, categoryId);
        return respondOkWithBody(result, getTraceId());
    }
}