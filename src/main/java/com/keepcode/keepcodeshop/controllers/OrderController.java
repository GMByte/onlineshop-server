package com.keepcode.keepcodeshop.controllers;

import com.keepcode.keepcodeshop.dto.views.BaseResponse;
import com.keepcode.keepcodeshop.dto.views.order.OrderCreateView;
import com.keepcode.keepcodeshop.dto.views.order.OrderUpdateView;
import com.keepcode.keepcodeshop.dto.views.order.ProductToOrderView;
import com.keepcode.keepcodeshop.exceptions.ApiException;
import com.keepcode.keepcodeshop.exceptions.RequestValidationException;
import com.keepcode.keepcodeshop.services.OrderService;
import com.keepcode.keepcodeshop.services.UserService;
import lombok.var;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.ValidationException;

import static com.keepcode.keepcodeshop.constants.UrlConstants.CREATE_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.DELETE_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.GET_ALL_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.ORDER_BASE_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.READ_OPERATION_PREFIX;
import static com.keepcode.keepcodeshop.constants.UrlConstants.UPDATE_REQUEST_PATH;
import static com.keepcode.keepcodeshop.constants.UrlConstants.WRITE_OPERATION_PREFIX;
import static com.keepcode.keepcodeshop.tracing.TraceIdContextHolder.getTraceId;
import static com.keepcode.keepcodeshop.utils.ResponseUtils.respondOk;
import static com.keepcode.keepcodeshop.utils.ResponseUtils.respondOkWithBody;
import static com.keepcode.keepcodeshop.utils.ResponseUtils.respondOkWithId;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/v1")
public class OrderController extends AuthenticatedUserController {

    private static final String WRITE_ORDER_BASE_PATH = ORDER_BASE_PATH + WRITE_OPERATION_PREFIX;
    private static final String READ_ORDER_BASE_PATH = ORDER_BASE_PATH + READ_OPERATION_PREFIX;

    public static final String DELETE_PRODUCT_FROM_ORDER_PATH = "/{id}/delete_product/{product_id}";
    public static final String ADD_PRODUCT_TO_ORDER = "/add_product_to_order";
    public static final String GET_ORDERS_BY_USER_ID = "/by/user/{user_id}";
    public static final String GET_ORDERS_BY_CLIENT_ID = "/by/client/{client_id}";
    public static final String CANCEL_ORDER = "/cancel";

    private final OrderService orderService;

    public OrderController(UserService userService, OrderService orderService) {
        super(userService);
        this.orderService = orderService;
    }

    @PostMapping(path = WRITE_ORDER_BASE_PATH + CREATE_REQUEST_PATH, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse create(@RequestBody OrderCreateView view) {
        var result = orderService.create(view, getLoggedInUser());
        return respondOkWithId(result.getId(), getTraceId());
    }

    @PostMapping(path = WRITE_ORDER_BASE_PATH + UPDATE_REQUEST_PATH, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse update(@RequestBody OrderUpdateView view) {
        orderService.update(view);
        return respondOk(getTraceId());
    }

    @PostMapping(path = WRITE_ORDER_BASE_PATH + ADD_PRODUCT_TO_ORDER, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse addProductToOrder(@RequestBody ProductToOrderView view) throws RequestValidationException {
        orderService.addProductToOrder(view, getLoggedInUser());
        return respondOk(getTraceId());
    }

    @GetMapping(path = READ_ORDER_BASE_PATH + GET_ALL_REQUEST_PATH, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse listOrdersForUser() {
        var result = orderService.listOrders(getLoggedInUser());
        return respondOkWithBody(result, getTraceId());
    }

    @DeleteMapping(path = WRITE_ORDER_BASE_PATH + CANCEL_ORDER, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse cancel(@RequestParam(value = "id") Long id) throws RequestValidationException {
        orderService.cancel(id, getLoggedInUser());
        return respondOk(getTraceId());
    }

    @DeleteMapping(path = WRITE_ORDER_BASE_PATH + DELETE_REQUEST_PATH, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody BaseResponse delete(@RequestParam(value = "id") Long id) {
        orderService.delete(id);
        return respondOk(getTraceId());
    }

    @DeleteMapping(path = WRITE_ORDER_BASE_PATH + DELETE_PRODUCT_FROM_ORDER_PATH, produces = APPLICATION_JSON_VALUE)
    public BaseResponse deleteProduct(@PathVariable(value = "id") Long id,
                                      @PathVariable(value = "product_id") Long productId) throws RequestValidationException {
        orderService.deleteProductFromOrder(id, productId, getLoggedInUser());
        return respondOk(getTraceId());
    }

    @GetMapping(path = READ_ORDER_BASE_PATH + GET_ORDERS_BY_USER_ID, produces = APPLICATION_JSON_VALUE)
    public BaseResponse getOrdersByUser(@PathVariable(value = "user_id") Long userId) {
        var result = orderService.findOrdersByUser(userId);
        return respondOkWithBody(result, getTraceId());
    }

    @GetMapping(path = READ_ORDER_BASE_PATH + GET_ORDERS_BY_CLIENT_ID, produces = APPLICATION_JSON_VALUE)
    public BaseResponse getOrdersByClient(@PathVariable(value = "client_id") Long clientId) {
        var result = orderService.findOrdersByClient(clientId);
        return respondOkWithBody(result, getTraceId());
    }
}