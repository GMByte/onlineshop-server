package com.keepcode.keepcodeshop.repositories;

import com.keepcode.keepcodeshop.dto.entities.Order;
import com.keepcode.keepcodeshop.dto.entities.RowStatus;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {
    @Query(value = "select ord from Order ord where ord.client.userId = :userId and ord.rowStatus.id <> :rowStatusId")
    List<Order> findOrdersByUserIdAndNotInStatus(Long userId, Long rowStatusId);

    @Query(value = "select ord from Order ord where ord.client.id = :clientId and ord.rowStatus.id <> :rowStatusId")
    List<Order> findOrdersByClientAndNotInStatus(Long clientId, Long rowStatusId);

    Optional<Order> getOrderByIdAndRowStatusNot(Long id, RowStatus rowStatus);
}