package com.keepcode.keepcodeshop.repositories;

import com.keepcode.keepcodeshop.dto.entities.Category;
import com.keepcode.keepcodeshop.dto.entities.Product;
import com.keepcode.keepcodeshop.dto.entities.RowStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {
    Optional<Product> findByIdAndAndRowStatusNot(Long id, RowStatus rowStatus);

    @Query("select p from Product p where (:categoryId is null or p.category.id = :categoryId) and p.rowStatus = :rowStatus")
    Page<Product> findProductsByCategoryOptional(Long categoryId, RowStatus rowStatus, Pageable page);

    List<Product> findProductsByCategory(Category category);
}
