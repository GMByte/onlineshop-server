package com.keepcode.keepcodeshop.repositories;

import com.keepcode.keepcodeshop.dto.entities.RowStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface RowStatusRepository extends JpaRepository<RowStatus, Long> {
    @Query(value = "select rs from RowStatus rs where rs.code = :statusCode")
    Optional<RowStatus> getByCode(String statusCode);
}