package com.keepcode.keepcodeshop.repositories;

import com.keepcode.keepcodeshop.dto.entities.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Long> {}
