package com.keepcode.keepcodeshop.repositories;

import com.keepcode.keepcodeshop.dto.entities.ProductsInOrders;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductOrderRepository extends CrudRepository<ProductsInOrders, ProductsInOrders.OrderProductKey> {
    @Query(value = "select pio from ProductsInOrders pio where pio.orderProductKey.order.id = :orderId")
    List<ProductsInOrders> getProductsByOrderId(Long orderId);
}
