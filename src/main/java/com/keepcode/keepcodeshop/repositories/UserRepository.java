package com.keepcode.keepcodeshop.repositories;

import com.keepcode.keepcodeshop.dto.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User getUserByName(String name);
}