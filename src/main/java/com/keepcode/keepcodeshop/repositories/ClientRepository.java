package com.keepcode.keepcodeshop.repositories;

import com.keepcode.keepcodeshop.dto.entities.Client;
import com.keepcode.keepcodeshop.dto.entities.RowStatus;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ClientRepository extends CrudRepository<Client, Long> {
    List<Client> getClientsByUserIdAndRowStatusNot(Long userId, RowStatus rowStatus);

    List<Client> getClientsByRowStatusNot(RowStatus rowStatus);

    Optional<Client> getClientByIdAndRowStatusNot(Long id, RowStatus rowStatus);
}