-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost    Database: keepcode_shop
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `ctgr_id` int NOT NULL COMMENT 'Технический первичный ключ',
  `ctgr_name` varchar(45) NOT NULL COMMENT 'Имя категории',
  PRIMARY KEY (`ctgr_id`),
  UNIQUE KEY `ctgr_name_UNIQUE` (`ctgr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Таблица содержит категории продаваемых магазином товаров';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clients` (
  `clnt_id` int NOT NULL AUTO_INCREMENT COMMENT 'Технический первичный ключ',
  `usr_id` int NOT NULL COMMENT 'Ссылка на таблицу users',
  `clnt_full_name` varchar(100) NOT NULL COMMENT 'Полное имя клиента',
  `clnt_contact_phone` varchar(25) NOT NULL COMMENT 'Контактный телефон клиента',
  `clnt_add_date` datetime NOT NULL COMMENT 'Дата добавления клиента',
  `rsts_id` int NOT NULL,
  PRIMARY KEY (`clnt_id`),
  KEY `fk_clnt_usr_id_idx` (`usr_id`),
  KEY `fk_clnt_rsts_id` (`rsts_id`),
  CONSTRAINT `fk_clnt_rsts_id` FOREIGN KEY (`rsts_id`) REFERENCES `row_statuses` (`rsts_id`),
  CONSTRAINT `fk_clnt_usr_id` FOREIGN KEY (`usr_id`) REFERENCES `users` (`usr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Таблица хранит данные о клиентах';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `ordr_id` int NOT NULL AUTO_INCREMENT COMMENT 'Технический первичный ключ',
  `clnt_id` int NOT NULL COMMENT 'Ссылка на таблицу clients',
  `ordr_price_total` double NOT NULL COMMENT 'Сумма заказа',
  `ordr_add_date` datetime NOT NULL COMMENT 'Дата создания заказа',
  `ordr_number` varchar(45) NOT NULL COMMENT 'Номер заказа',
  `ordr_post_number` varchar(50) DEFAULT NULL COMMENT 'Трек-номер заказа (если есть)',
  `rsts_id` bigint DEFAULT NULL,
  PRIMARY KEY (`ordr_id`),
  KEY `FKr1vlrmwphu95sfqdj29rs86ae` (`clnt_id`),
  CONSTRAINT `FKr1vlrmwphu95sfqdj29rs86ae` FOREIGN KEY (`clnt_id`) REFERENCES `clients` (`clnt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Таблица содержит заказы магазина';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `producs_to_orders`
--

DROP TABLE IF EXISTS `producs_to_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `producs_to_orders` (
  `ordr_id` int NOT NULL COMMENT 'Ссылка на таблицу orders',
  `prdt_id` int NOT NULL COMMENT 'Ссылка на таблицу products',
  `prdt_ordr_amount` double NOT NULL COMMENT 'Количество единицы в заказе',
  KEY `fk_ordr_id_idx` (`ordr_id`),
  KEY `fk_prdts_Id_idx` (`prdt_id`),
  CONSTRAINT `fk_prdt_ordr_ordr_id` FOREIGN KEY (`ordr_id`) REFERENCES `orders` (`ordr_id`),
  CONSTRAINT `fk_prdt_ordr_prdt_id` FOREIGN KEY (`prdt_id`) REFERENCES `products` (`prdt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Таблица реализует связь "многие-ко-многим" для товаров и заказов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `prdt_id` int NOT NULL COMMENT 'Технический первичный ключ\\n',
  `prdt_name` varchar(80) NOT NULL COMMENT 'Название продукта\\n',
  `ctgr_id` int DEFAULT NULL COMMENT 'Ссылка на категорию продукта',
  `prdt_balance_units` double NOT NULL COMMENT 'Остаток продукта на складе',
  `prdt_measurement_units` varchar(5) NOT NULL COMMENT 'Мера измерения продукта (шт., кг., и т.п.)\\n',
  `rsts_id` int NOT NULL,
  `prdt_price` double DEFAULT NULL,
  PRIMARY KEY (`prdt_id`),
  UNIQUE KEY `prdts_name_UNIQUE` (`prdt_name`),
  KEY `fk_prdts_ctgr_id_idx` (`ctgr_id`),
  KEY `fk_prdts_rsts_Id_idx` (`rsts_id`),
  CONSTRAINT `fk_prdts_ctgr_id` FOREIGN KEY (`ctgr_id`) REFERENCES `categories` (`ctgr_id`),
  CONSTRAINT `fk_prdts_rsts_id` FOREIGN KEY (`rsts_id`) REFERENCES `row_statuses` (`rsts_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Таблица содержит список товаров продаваемых магазином';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `products_to_orders`
--

DROP TABLE IF EXISTS `products_to_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products_to_orders` (
  `prdt_ordr_amount` double DEFAULT NULL,
  `prdt_id` bigint NOT NULL,
  `ordr_id` bigint NOT NULL,
  PRIMARY KEY (`ordr_id`,`prdt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `row_statuses`
--

DROP TABLE IF EXISTS `row_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `row_statuses` (
  `rsts_id` int NOT NULL AUTO_INCREMENT COMMENT 'Технический первичный ключ',
  `rsts_code` varchar(45) NOT NULL COMMENT 'Буквенное обозначение статуса',
  PRIMARY KEY (`rsts_id`),
  UNIQUE KEY `rsts_code_UNIQUE` (`rsts_code`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Таблица хранит статусы строк';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `row_statuses`
--

LOCK TABLES `row_statuses` WRITE;
/*!40000 ALTER TABLE `row_statuses` DISABLE KEYS */;
INSERT INTO `row_statuses` VALUES (1,'ACTIVE'),(3,'CREATED'),(2,'DELETED'),(6,'MANUAL'),(7,'RECEIVED'),(5,'SENT');
/*!40000 ALTER TABLE `row_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `usr_id` int NOT NULL AUTO_INCREMENT COMMENT 'Технический первичный ключ',
  `usr_name` varchar(45) NOT NULL COMMENT 'Логин пользователя (email)',
  `usr_password` varchar(100) NOT NULL COMMENT 'Пароль пользователя в зашифрованном виде',
  `usr_role` varchar(45) NOT NULL COMMENT 'Роль ползователя (A - администратор, M - мэнеджер, C - клиент)',
  `usr_add_date` datetime NOT NULL COMMENT 'Дата добавления пользователя',
  `usr_last_login_date` datetime DEFAULT NULL COMMENT 'Дата последнего входа пользователя',
  PRIMARY KEY (`usr_id`),
  UNIQUE KEY `users_usr_name_uindex` (`usr_name`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Таблица хранит данные о пользователях';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (null,'admin','$2y$10$NDy9h7KafZfQ2joMkIws1.tsWEvhuwLidL/WHaI4wNhdSWDwm.//G','ADMIN', NOW(),null);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-07  2:50:57
